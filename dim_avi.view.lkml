view: dim_avi {
  sql_table_name: CTMR_MART.DIM_AVI ;;

  dimension: antenna_primary_dir_cd {
    type: number
    sql: ${TABLE}.ANTENNA_PRIMARY_DIR_CD ;;
  }

  dimension: antenna_primary_dir_name {
    type: string
    sql: ${TABLE}.ANTENNA_PRIMARY_DIR_NAME ;;
  }

  dimension: antenna_primary_dir_value {
    type: string
    sql: ${TABLE}.ANTENNA_PRIMARY_DIR_VALUE ;;
  }

  dimension: antenna_secondary_dir_cd {
    type: number
    sql: ${TABLE}.ANTENNA_SECONDARY_DIR_CD ;;
  }

  dimension: antenna_secondary_dir_name {
    type: string
    sql: ${TABLE}.ANTENNA_SECONDARY_DIR_NAME ;;
  }

  dimension: antenna_secondary_dir_value {
    type: string
    sql: ${TABLE}.ANTENNA_SECONDARY_DIR_VALUE ;;
  }

  dimension: avi_id {
    type: number
    sql: ${TABLE}.AVI_ID ;;
  }

  dimension: avi_reader_id_txt {
    type: string
    sql: ${TABLE}.AVI_READER_ID_TXT ;;
  }

  dimension: avi_sk {
    type: number
    sql: ${TABLE}.AVI_SK ;;
  }

  dimension: comm_error_flg {
    type: number
    sql: ${TABLE}.COMM_ERROR_FLG ;;
  }

  dimension: common_name_txt {
    type: string
    sql: ${TABLE}.COMMON_NAME_TXT ;;
  }

  dimension: configured_lanes_num {
    type: number
    sql: ${TABLE}.CONFIGURED_LANES_NUM ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension_group: last_collection_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_COLLECTION_DT ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: type_avi_model_cd {
    type: number
    sql: ${TABLE}.TYPE_AVI_MODEL_CD ;;
  }

  dimension: type_avi_model_desc {
    type: string
    sql: ${TABLE}.TYPE_AVI_MODEL_DESC ;;
  }

  dimension: type_avi_status_cd {
    type: number
    sql: ${TABLE}.TYPE_AVI_STATUS_CD ;;
  }

  dimension: type_avi_status_desc {
    type: string
    sql: ${TABLE}.TYPE_AVI_STATUS_DESC ;;
  }

  dimension: type_avi_tech_cd {
    type: number
    sql: ${TABLE}.TYPE_AVI_TECH_CD ;;
  }

  dimension: type_avi_tech_desc {
    type: string
    sql: ${TABLE}.TYPE_AVI_TECH_DESC ;;
  }

  dimension: type_device_cd {
    type: number
    sql: ${TABLE}.TYPE_DEVICE_CD ;;
  }

  dimension: type_device_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DEVICE_SHORT_DESC ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_SHORT_DESC ;;
  }

  dimension: type_reason_cd {
    type: number
    sql: ${TABLE}.TYPE_REASON_CD ;;
  }

  dimension: type_reason_desc {
    type: string
    sql: ${TABLE}.TYPE_REASON_DESC ;;
  }

  dimension: type_road_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_CD ;;
  }

  dimension: type_road_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC ;;
  }

  dimension: type_road_short_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_SHORT_DESC ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  measure: count {
    type: count
    drill_fields: [antenna_primary_dir_name, antenna_secondary_dir_name]
  }
}
