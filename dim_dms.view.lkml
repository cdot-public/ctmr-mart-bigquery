view: dim_dms {
  sql_table_name: CTMR_MART.DIM_DMS ;;

  dimension: airflow_fans_num {
    type: number
    sql: ${TABLE}.AIRFLOW_FANS_NUM ;;
  }

  dimension: airflow_filters_num {
    type: number
    sql: ${TABLE}.AIRFLOW_FILTERS_NUM ;;
  }

  dimension: airflow_sensors_num {
    type: number
    sql: ${TABLE}.AIRFLOW_SENSORS_NUM ;;
  }

  dimension: ambient_temps_num {
    type: number
    sql: ${TABLE}.AMBIENT_TEMPS_NUM ;;
  }

  dimension: border_size_horizontal_num {
    type: number
    sql: ${TABLE}.BORDER_SIZE_HORIZONTAL_NUM ;;
  }

  dimension: border_size_vertical_num {
    type: number
    sql: ${TABLE}.BORDER_SIZE_VERTICAL_NUM ;;
  }

  dimension: brightness_levels_num {
    type: number
    sql: ${TABLE}.BRIGHTNESS_LEVELS_NUM ;;
  }

  dimension: columns_num {
    type: number
    sql: ${TABLE}.COLUMNS_NUM ;;
  }

  dimension: comm_level_num {
    type: number
    sql: ${TABLE}.COMM_LEVEL_NUM ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: current_brightness_id {
    type: number
    sql: ${TABLE}.CURRENT_BRIGHTNESS_ID ;;
  }

  dimension: current_message_id {
    type: number
    sql: ${TABLE}.CURRENT_MESSAGE_ID ;;
  }

  dimension: current_pixel_test_id {
    type: number
    sql: ${TABLE}.CURRENT_PIXEL_TEST_ID ;;
  }

  dimension: current_poll_id {
    type: number
    sql: ${TABLE}.CURRENT_POLL_ID ;;
  }

  dimension: default_bg_color_rgb_txt {
    type: string
    sql: ${TABLE}.DEFAULT_BG_COLOR_RGB_TXT ;;
  }

  dimension: default_fg_color_rgb_txt {
    type: string
    sql: ${TABLE}.DEFAULT_FG_COLOR_RGB_TXT ;;
  }

  dimension: default_multistring_txt {
    type: string
    sql: ${TABLE}.DEFAULT_MULTISTRING_TXT ;;
  }

  dimension: default_page_offtime_num {
    type: number
    sql: ${TABLE}.DEFAULT_PAGE_OFFTIME_NUM ;;
  }

  dimension: default_page_ontime_num {
    type: number
    sql: ${TABLE}.DEFAULT_PAGE_ONTIME_NUM ;;
  }

  dimension: default_txt_flash_timeoff_num {
    type: number
    sql: ${TABLE}.DEFAULT_TXT_FLASH_TIMEOFF_NUM ;;
  }

  dimension: default_txt_flash_timeon_num {
    type: number
    sql: ${TABLE}.DEFAULT_TXT_FLASH_TIMEON_NUM ;;
  }

  dimension: device_id {
    type: number
    sql: ${TABLE}.DEVICE_ID ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: dms_id {
    type: number
    sql: ${TABLE}.DMS_ID ;;
  }

  dimension: dms_sk {
    type: number
    sql: ${TABLE}.DMS_SK ;;
  }

  dimension: door_sensors_num {
    type: number
    sql: ${TABLE}.DOOR_SENSORS_NUM ;;
  }

  dimension: horizontal_pixel_pitch_num {
    type: number
    sql: ${TABLE}.HORIZONTAL_PIXEL_PITCH_NUM ;;
  }

  dimension: include_speed_message_flg {
    type: number
    sql: ${TABLE}.INCLUDE_SPEED_MESSAGE_FLG ;;
  }

  dimension: include_speed_message_id {
    type: number
    sql: ${TABLE}.INCLUDE_SPEED_MESSAGE_ID ;;
  }

  dimension: include_speed_message_short {
    type: string
    sql: ${TABLE}.INCLUDE_SPEED_MESSAGE_SHORT ;;
  }

  dimension: include_speed_message_txt {
    type: string
    sql: ${TABLE}.INCLUDE_SPEED_MESSAGE_TXT ;;
  }

  dimension: internal_temps_num {
    type: number
    sql: ${TABLE}.INTERNAL_TEMPS_NUM ;;
  }

  dimension: lane_controller_id {
    type: number
    sql: ${TABLE}.LANE_CONTROLLER_ID ;;
  }

  dimension: lane_num {
    type: number
    sql: ${TABLE}.LANE_NUM ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: legend_exists_flg {
    type: number
    sql: ${TABLE}.LEGEND_EXISTS_FLG ;;
  }

  dimension: library_id {
    type: number
    sql: ${TABLE}.LIBRARY_ID ;;
  }

  dimension: lines_num {
    type: number
    sql: ${TABLE}.LINES_NUM ;;
  }

  dimension: max_graphic_size_num {
    type: number
    sql: ${TABLE}.MAX_GRAPHIC_SIZE_NUM ;;
  }

  dimension: max_graphics_num {
    type: number
    sql: ${TABLE}.MAX_GRAPHICS_NUM ;;
  }

  dimension: max_pages_num {
    type: number
    sql: ${TABLE}.MAX_PAGES_NUM ;;
  }

  dimension: message_expiry_interval_num {
    type: number
    sql: ${TABLE}.MESSAGE_EXPIRY_INTERVAL_NUM ;;
  }

  dimension: message_prefix_txt {
    type: string
    sql: ${TABLE}.MESSAGE_PREFIX_TXT ;;
  }

  dimension: modules_per_line_num {
    type: number
    sql: ${TABLE}.MODULES_PER_LINE_NUM ;;
  }

  dimension: photocell_levels_num {
    type: number
    sql: ${TABLE}.PHOTOCELL_LEVELS_NUM ;;
  }

  dimension: photocells_num {
    type: number
    sql: ${TABLE}.PHOTOCELLS_NUM ;;
  }

  dimension: physical_height_num {
    type: number
    sql: ${TABLE}.PHYSICAL_HEIGHT_NUM ;;
  }

  dimension: physical_width_num {
    type: number
    sql: ${TABLE}.PHYSICAL_WIDTH_NUM ;;
  }

  dimension: pixel_height_num {
    type: number
    sql: ${TABLE}.PIXEL_HEIGHT_NUM ;;
  }

  dimension: pixel_service_duration_num {
    type: number
    sql: ${TABLE}.PIXEL_SERVICE_DURATION_NUM ;;
  }

  dimension: pixel_service_interval_num {
    type: number
    sql: ${TABLE}.PIXEL_SERVICE_INTERVAL_NUM ;;
  }

  dimension: pixel_service_synch_num {
    type: number
    sql: ${TABLE}.PIXEL_SERVICE_SYNCH_NUM ;;
  }

  dimension: pixel_width_num {
    type: number
    sql: ${TABLE}.PIXEL_WIDTH_NUM ;;
  }

  dimension: pixels_per_sign_pixel_num {
    type: number
    sql: ${TABLE}.PIXELS_PER_SIGN_PIXEL_NUM ;;
  }

  dimension: power_supplies_num {
    type: number
    sql: ${TABLE}.POWER_SUPPLIES_NUM ;;
  }

  dimension: rows_num {
    type: number
    sql: ${TABLE}.ROWS_NUM ;;
  }

  dimension: surge_power_level_num {
    type: number
    sql: ${TABLE}.SURGE_POWER_LEVEL_NUM ;;
  }

  dimension: type_default_bg_color_cd {
    type: number
    sql: ${TABLE}.TYPE_DEFAULT_BG_COLOR_CD ;;
  }

  dimension: type_default_fg_color_cd {
    type: number
    sql: ${TABLE}.TYPE_DEFAULT_FG_COLOR_CD ;;
  }

  dimension: type_dms_beacon_cd {
    type: number
    sql: ${TABLE}.TYPE_DMS_BEACON_CD ;;
  }

  dimension: type_dms_cd {
    type: number
    sql: ${TABLE}.TYPE_DMS_CD ;;
  }

  dimension: type_dms_code_value {
    type: string
    sql: ${TABLE}.TYPE_DMS_CODE_VALUE ;;
  }

  dimension: type_dms_color_scheme_cd {
    type: number
    sql: ${TABLE}.TYPE_DMS_COLOR_SCHEME_CD ;;
  }

  dimension: type_dms_long_desc {
    type: string
    sql: ${TABLE}.TYPE_DMS_LONG_DESC ;;
  }

  dimension: type_dms_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DMS_SHORT_DESC ;;
  }

  dimension: type_dms_sort_order {
    type: number
    sql: ${TABLE}.TYPE_DMS_SORT_ORDER ;;
  }

  dimension: type_dms_status_cd {
    type: number
    sql: ${TABLE}.TYPE_DMS_STATUS_CD ;;
  }

  dimension: type_dms_status_code_value {
    type: string
    sql: ${TABLE}.TYPE_DMS_STATUS_CODE_VALUE ;;
  }

  dimension: type_dms_status_long_desc {
    type: string
    sql: ${TABLE}.TYPE_DMS_STATUS_LONG_DESC ;;
  }

  dimension: type_dms_status_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DMS_STATUS_SHORT_DESC ;;
  }

  dimension: type_dms_status_sort_order {
    type: number
    sql: ${TABLE}.TYPE_DMS_STATUS_SORT_ORDER ;;
  }

  dimension: type_dms_tech_cd {
    type: number
    sql: ${TABLE}.TYPE_DMS_TECH_CD ;;
  }

  dimension: type_font_cd {
    type: number
    sql: ${TABLE}.TYPE_FONT_CD ;;
  }

  dimension: type_font_group_cd {
    type: number
    sql: ${TABLE}.TYPE_FONT_GROUP_CD ;;
  }

  dimension: type_lane_cd {
    type: number
    sql: ${TABLE}.TYPE_LANE_CD ;;
  }

  dimension: type_lane_code_value {
    type: string
    sql: ${TABLE}.TYPE_LANE_CODE_VALUE ;;
  }

  dimension: type_lane_long_desc {
    type: string
    sql: ${TABLE}.TYPE_LANE_LONG_DESC ;;
  }

  dimension: type_lane_short_desc {
    type: string
    sql: ${TABLE}.TYPE_LANE_SHORT_DESC ;;
  }

  dimension: type_lane_sort_order {
    type: number
    sql: ${TABLE}.TYPE_LANE_SORT_ORDER ;;
  }

  dimension: type_line_justification_cd {
    type: number
    sql: ${TABLE}.TYPE_LINE_JUSTIFICATION_CD ;;
  }

  dimension: type_matrix_cd {
    type: number
    sql: ${TABLE}.TYPE_MATRIX_CD ;;
  }

  dimension: type_message_memory_cd {
    type: number
    sql: ${TABLE}.TYPE_MESSAGE_MEMORY_CD ;;
  }

  dimension: type_ntcip_version_cd {
    type: number
    sql: ${TABLE}.TYPE_NTCIP_VERSION_CD ;;
  }

  dimension: type_page_justification_cd {
    type: number
    sql: ${TABLE}.TYPE_PAGE_JUSTIFICATION_CD ;;
  }

  dimension: type_priority_message_cd {
    type: number
    sql: ${TABLE}.TYPE_PRIORITY_MESSAGE_CD ;;
  }

  dimension: type_priority_message_long {
    type: string
    sql: ${TABLE}.TYPE_PRIORITY_MESSAGE_LONG ;;
  }

  dimension: type_priority_message_short {
    type: string
    sql: ${TABLE}.TYPE_PRIORITY_MESSAGE_SHORT ;;
  }

  dimension: type_priority_message_sort {
    type: number
    sql: ${TABLE}.TYPE_PRIORITY_MESSAGE_SORT ;;
  }

  dimension: type_priority_message_value {
    type: string
    sql: ${TABLE}.TYPE_PRIORITY_MESSAGE_VALUE ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.USERNAME ;;
  }

  dimension: vertical_pixel_pitch_num {
    type: number
    sql: ${TABLE}.VERTICAL_PIXEL_PITCH_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: [username]
  }
}
