view: fact_dms_message {
  sql_table_name: CTMR_MART.FACT_DMS_MESSAGE ;;

  dimension: active_message_error_num {
    type: number
    sql: ${TABLE}.ACTIVE_MESSAGE_ERROR_NUM ;;
  }

  dimension: alt_message_txt {
    type: string
    sql: ${TABLE}.ALT_MESSAGE_TXT ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dms_message_id {
    type: number
    sql: ${TABLE}.DMS_MESSAGE_ID ;;
  }

  dimension: dms_message_short {
    type: string
    sql: ${TABLE}.DMS_MESSAGE_SHORT ;;
  }

  dimension: dms_message_sk {
    type: number
    sql: ${TABLE}.DMS_MESSAGE_SK ;;
  }

  dimension: dms_sk {
    type: number
    sql: ${TABLE}.DMS_SK ;;
  }

  dimension: duration_num {
    type: number
    sql: ${TABLE}.DURATION_NUM ;;
  }

  dimension_group: end_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.END_DT ;;
  }

  dimension: end_dt_sk {
    type: number
    sql: ${TABLE}.END_DT_SK ;;
  }

  dimension: end_tm_sk {
    type: number
    sql: ${TABLE}.END_TM_SK ;;
  }

  dimension: event_sk {
    type: number
    sql: ${TABLE}.EVENT_SK ;;
  }

  dimension_group: expire_time_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.EXPIRE_TIME_DT ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension_group: future_activation_time_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FUTURE_ACTIVATION_TIME_DT ;;
  }

  dimension: has_graphics_flg {
    type: number
    sql: ${TABLE}.HAS_GRAPHICS_FLG ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: message_crc_num {
    type: number
    sql: ${TABLE}.MESSAGE_CRC_NUM ;;
  }

  dimension: message_pixel_service_flg {
    type: number
    sql: ${TABLE}.MESSAGE_PIXEL_SERVICE_FLG ;;
  }

  dimension: message_txt {
    type: string
    sql: ${TABLE}.MESSAGE_TXT ;;
  }

  dimension: msg_activated_by_txt {
    type: string
    sql: ${TABLE}.MSG_ACTIVATED_BY_TXT ;;
  }

  dimension: multi_syn_err_position_txt {
    type: string
    sql: ${TABLE}.MULTI_SYN_ERR_POSITION_TXT ;;
  }

  dimension: multi_syn_err_txt {
    type: string
    sql: ${TABLE}.MULTI_SYN_ERR_TXT ;;
  }

  dimension: override_flg {
    type: number
    sql: ${TABLE}.OVERRIDE_FLG ;;
  }

  dimension: page_off_num {
    type: number
    sql: ${TABLE}.PAGE_OFF_NUM ;;
  }

  dimension: page_on_num {
    type: number
    sql: ${TABLE}.PAGE_ON_NUM ;;
  }

  dimension: page_time_num {
    type: number
    sql: ${TABLE}.PAGE_TIME_NUM ;;
  }

  dimension_group: remind_time_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.REMIND_TIME_DT ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension_group: sign_activation_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.SIGN_ACTIVATION_DT ;;
  }

  dimension: source_ip_address_txt {
    type: string
    sql: ${TABLE}.SOURCE_IP_ADDRESS_TXT ;;
  }

  dimension: speed_msg_flg {
    type: number
    sql: ${TABLE}.SPEED_MSG_FLG ;;
  }

  dimension: spell_check_flg {
    type: number
    sql: ${TABLE}.SPELL_CHECK_FLG ;;
  }

  dimension_group: start_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.START_DT ;;
  }

  dimension: start_dt_sk {
    type: number
    sql: ${TABLE}.START_DT_SK ;;
  }

  dimension: start_tm_sk {
    type: number
    sql: ${TABLE}.START_TM_SK ;;
  }

  dimension: toll_msg_flg {
    type: number
    sql: ${TABLE}.TOLL_MSG_FLG ;;
  }

  dimension: type_priority_message_sk {
    type: number
    sql: ${TABLE}.TYPE_PRIORITY_MESSAGE_SK ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
