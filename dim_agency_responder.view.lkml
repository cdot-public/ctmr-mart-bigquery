view: dim_agency_responder {
  sql_table_name: CTMR_MART.DIM_AGENCY_RESPONDER ;;

  dimension: agency_responder_id {
    type: number
    sql: ${TABLE}.AGENCY_RESPONDER_ID ;;
  }

  dimension: agency_responder_sk {
    type: number
    sql: ${TABLE}.AGENCY_RESPONDER_SK ;;
  }

  dimension: agency_responder_type_id {
    type: number
    sql: ${TABLE}.AGENCY_RESPONDER_TYPE_ID ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_begin {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_BEGIN_DATE ;;
  }

  dimension_group: dim_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_END_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension: responder_desc_txt {
    type: string
    sql: ${TABLE}.RESPONDER_DESC_TXT ;;
  }

  dimension: responder_id_txt {
    type: string
    sql: ${TABLE}.RESPONDER_ID_TXT ;;
  }

  dimension: type_agency_dispatch_cd {
    type: number
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_CD ;;
  }

  dimension: type_agency_dispatch_long {
    type: string
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_LONG ;;
  }

  dimension: type_agency_dispatch_short {
    type: string
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_SHORT ;;
  }

  dimension: type_agency_dispatch_sort {
    type: number
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_SORT ;;
  }

  dimension: type_agency_dispatch_value {
    type: string
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_VALUE ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
