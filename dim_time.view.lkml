view: dim_time {
  sql_table_name: CTMR_MART.DIM_TIME ;;

  dimension: am_pm {
    type: string
    sql: ${TABLE}.AM_PM ;;
  }

  dimension: grp_15_min {
    type: string
    sql: ${TABLE}.GRP_15_MIN ;;
  }

  dimension: grp_1_min {
    type: string
    sql: ${TABLE}.GRP_1_MIN ;;
  }

  dimension: grp_30_min {
    type: string
    sql: ${TABLE}.GRP_30_MIN ;;
  }

  dimension: grp_5_min {
    type: string
    sql: ${TABLE}.GRP_5_MIN ;;
  }

  dimension: grp_hour {
    type: string
    sql: ${TABLE}.GRP_HOUR ;;
  }

  dimension: hour_12 {
    type: number
    sql: ${TABLE}.HOUR_12 ;;
  }

  dimension: hour_24 {
    type: number
    sql: ${TABLE}.HOUR_24 ;;
  }

  dimension: time_12 {
    type: string
    sql: ${TABLE}.TIME_12 ;;
  }

  dimension: time_24 {
    type: string
    sql: ${TABLE}.TIME_24 ;;
  }

  dimension: time_min {
    type: number
    sql: ${TABLE}.TIME_MIN ;;
  }

  dimension: time_sec {
    type: number
    sql: ${TABLE}.TIME_SEC ;;
  }

  dimension: time_sk {
    type: number
    sql: ${TABLE}.TIME_SK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
