view: dim_calendar {
  sql_table_name: CTMR_MART.DIM_CALENDAR ;;

  dimension: date_sk {
    type: number
    sql: ${TABLE}.DATE_SK ;;
  }

  dimension_group: date_time_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DATE_TIME_END ;;
  }

  dimension_group: date_time_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DATE_TIME_START ;;
  }

  dimension: date_value {
    type: string
    sql: ${TABLE}.DATE_VALUE ;;
  }

  dimension: day_of_month_number {
    type: number
    sql: ${TABLE}.DAY_OF_MONTH_NUMBER ;;
  }

  dimension: day_of_quarter_number {
    type: number
    sql: ${TABLE}.DAY_OF_QUARTER_NUMBER ;;
  }

  dimension: day_of_week_desc {
    type: string
    sql: ${TABLE}.DAY_OF_WEEK_DESC ;;
  }

  dimension: day_of_week_number {
    type: number
    sql: ${TABLE}.DAY_OF_WEEK_NUMBER ;;
  }

  dimension: day_of_week_sdesc {
    type: string
    sql: ${TABLE}.DAY_OF_WEEK_SDESC ;;
  }

  dimension: day_of_year_number {
    type: number
    sql: ${TABLE}.DAY_OF_YEAR_NUMBER ;;
  }

  dimension: days_in_month {
    type: number
    sql: ${TABLE}.DAYS_IN_MONTH ;;
  }

  dimension: days_in_quarter {
    type: number
    sql: ${TABLE}.DAYS_IN_QUARTER ;;
  }

  dimension: days_in_year {
    type: number
    sql: ${TABLE}.DAYS_IN_YEAR ;;
  }

  dimension: holiday_flag {
    type: string
    sql: ${TABLE}.HOLIDAY_FLAG ;;
  }

  dimension_group: iso_week_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ISO_WEEK_END_DATE ;;
  }

  dimension: iso_week_number {
    type: number
    sql: ${TABLE}.ISO_WEEK_NUMBER ;;
  }

  dimension_group: iso_week_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ISO_WEEK_START_DATE ;;
  }

  dimension: last_day_of_month_flag {
    type: number
    sql: ${TABLE}.LAST_DAY_OF_MONTH_FLAG ;;
  }

  dimension: last_day_of_quarter_flag {
    type: number
    sql: ${TABLE}.LAST_DAY_OF_QUARTER_FLAG ;;
  }

  dimension: month_desc {
    type: string
    sql: ${TABLE}.MONTH_DESC ;;
  }

  dimension_group: month_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.MONTH_END_DATE ;;
  }

  dimension: month_sdesc {
    type: string
    sql: ${TABLE}.MONTH_SDESC ;;
  }

  dimension_group: month_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.MONTH_START_DATE ;;
  }

  dimension: month_value {
    type: string
    sql: ${TABLE}.MONTH_VALUE ;;
  }

  dimension: quarter_desc {
    type: string
    sql: ${TABLE}.QUARTER_DESC ;;
  }

  dimension_group: quarter_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.QUARTER_END_DATE ;;
  }

  dimension_group: quarter_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.QUARTER_START_DATE ;;
  }

  dimension: quarter_value {
    type: string
    sql: ${TABLE}.QUARTER_VALUE ;;
  }

  dimension_group: week_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.WEEK_END_DATE ;;
  }

  dimension: week_in_month_number {
    type: number
    sql: ${TABLE}.WEEK_IN_MONTH_NUMBER ;;
  }

  dimension: week_in_year_number {
    type: number
    sql: ${TABLE}.WEEK_IN_YEAR_NUMBER ;;
  }

  dimension_group: week_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.WEEK_START_DATE ;;
  }

  dimension: weekend_flag {
    type: number
    sql: ${TABLE}.WEEKEND_FLAG ;;
  }

  dimension: year_desc {
    type: string
    sql: ${TABLE}.YEAR_DESC ;;
  }

  dimension_group: year_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.YEAR_END_DATE ;;
  }

  dimension: year_sdesc {
    type: string
    sql: ${TABLE}.YEAR_SDESC ;;
  }

  dimension_group: year_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.YEAR_START_DATE ;;
  }

  dimension: year_value {
    type: string
    sql: ${TABLE}.YEAR_VALUE ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
