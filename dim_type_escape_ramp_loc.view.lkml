view: dim_type_escape_ramp_loc {
  sql_table_name: CTMR_MART.DIM_TYPE_ESCAPE_RAMP_LOC ;;

  dimension: code_value_txt {
    type: string
    sql: ${TABLE}.CODE_VALUE_TXT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: long_desc_txt {
    type: string
    sql: ${TABLE}.LONG_DESC_TXT ;;
  }

  dimension: short_desc_txt {
    type: string
    sql: ${TABLE}.SHORT_DESC_TXT ;;
  }

  dimension: sort_order_num {
    type: number
    sql: ${TABLE}.SORT_ORDER_NUM ;;
  }

  dimension: type_escape_ramp_loc_cd {
    type: number
    sql: ${TABLE}.TYPE_ESCAPE_RAMP_LOC_CD ;;
  }

  dimension: type_escape_ramp_loc_sk {
    type: number
    sql: ${TABLE}.TYPE_ESCAPE_RAMP_LOC_SK ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
