view: dim_its_staff {
  sql_table_name: CTMR_MART.DIM_ITS_STAFF ;;

  dimension: assigned_only {
    type: number
    sql: ${TABLE}.ASSIGNED_ONLY ;;
  }

  dimension: auto_refresh_rate {
    type: number
    sql: ${TABLE}.AUTO_REFRESH_RATE ;;
  }

  dimension: change_passwd {
    type: number
    sql: ${TABLE}.CHANGE_PASSWD ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATED ;;
  }

  dimension: daylight_saving {
    type: number
    sql: ${TABLE}.DAYLIGHT_SAVING ;;
  }

  dimension: default_paper_size {
    type: string
    sql: ${TABLE}.DEFAULT_PAPER_SIZE ;;
  }

  dimension: default_signature_type {
    type: string
    sql: ${TABLE}.DEFAULT_SIGNATURE_TYPE ;;
  }

  dimension: dept_id {
    type: number
    sql: ${TABLE}.DEPT_ID ;;
  }

  dimension: dept_name {
    type: string
    sql: ${TABLE}.DEPT_NAME ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.EMAIL ;;
  }

  dimension: empsapno {
    type: string
    sql: ${TABLE}.EMPSAPNO ;;
  }

  dimension: firstname {
    type: string
    sql: ${TABLE}.FIRSTNAME ;;
  }

  dimension: fullname {
    type: string
    sql: ${TABLE}.FULLNAME ;;
  }

  dimension: group_id {
    type: number
    sql: ${TABLE}.GROUP_ID ;;
  }

  dimension: isactive {
    type: number
    sql: ${TABLE}.ISACTIVE ;;
  }

  dimension: isadmin {
    type: number
    sql: ${TABLE}.ISADMIN ;;
  }

  dimension: isvisible {
    type: number
    sql: ${TABLE}.ISVISIBLE ;;
  }

  dimension_group: lastlogin {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LASTLOGIN ;;
  }

  dimension: lastname {
    type: string
    sql: ${TABLE}.LASTNAME ;;
  }

  dimension: max_page_size {
    type: number
    sql: ${TABLE}.MAX_PAGE_SIZE ;;
  }

  dimension: mobile {
    type: string
    sql: ${TABLE}.MOBILE ;;
  }

  dimension: onvacation {
    type: number
    sql: ${TABLE}.ONVACATION ;;
  }

  dimension: passwd {
    type: string
    sql: ${TABLE}.PASSWD ;;
  }

  dimension_group: passwdreset {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.PASSWDRESET ;;
  }

  dimension: phone {
    type: string
    sql: ${TABLE}.PHONE ;;
  }

  dimension: phone_ext {
    type: string
    sql: ${TABLE}.PHONE_EXT ;;
  }

  dimension: show_assigned_tickets {
    type: number
    sql: ${TABLE}.SHOW_ASSIGNED_TICKETS ;;
  }

  dimension: staff_id {
    type: number
    sql: ${TABLE}.STAFF_ID ;;
  }

  dimension: staff_sk {
    type: number
    sql: ${TABLE}.STAFF_SK ;;
  }

  dimension: timezone_id {
    type: number
    sql: ${TABLE}.TIMEZONE_ID ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.UPDATED ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.USERNAME ;;
  }

  measure: count {
    type: count
    drill_fields: [dept_name, firstname, lastname, username, fullname]
  }
}
