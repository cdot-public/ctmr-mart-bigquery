view: dim_tv_camera_polling {
  sql_table_name: CTMR_MART.DIM_TV_CAMERA_POLLING ;;

  dimension: camera_polling_sk {
    type: number
    sql: ${TABLE}.CAMERA_POLLING_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: fs_direction {
    type: string
    sql: ${TABLE}.FS_DIRECTION ;;
  }

  dimension: fs_lanes {
    type: number
    sql: ${TABLE}.FS_LANES ;;
  }

  dimension: ns_direction {
    type: string
    sql: ${TABLE}.NS_DIRECTION ;;
  }

  dimension: ns_lanes {
    type: number
    sql: ${TABLE}.NS_LANES ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
