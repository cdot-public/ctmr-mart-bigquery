view: dim_device_segment {
  sql_table_name: CTMR_MART.DIM_DEVICE_SEGMENT ;;

  dimension: device_segement_sk {
    type: number
    sql: ${TABLE}.DEVICE_SEGEMENT_SK ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: mm_end_flt {
    type: number
    sql: ${TABLE}.MM_END_FLT ;;
  }

  dimension: mm_start_flt {
    type: number
    sql: ${TABLE}.MM_START_FLT ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: segment_sk {
    type: number
    sql: ${TABLE}.SEGMENT_SK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
