view: dim_gantry_device {
  sql_table_name: CTMR_MART.DIM_GANTRY_DEVICE ;;

  dimension: corridor_id {
    type: number
    sql: ${TABLE}.CORRIDOR_ID ;;
  }

  dimension: device_id {
    type: number
    sql: ${TABLE}.DEVICE_ID ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: gantry_device_sk {
    type: number
    sql: ${TABLE}.GANTRY_DEVICE_SK ;;
  }

  dimension: gantry_id {
    type: number
    sql: ${TABLE}.GANTRY_ID ;;
  }

  dimension: gantry_name_txt {
    type: string
    sql: ${TABLE}.GANTRY_NAME_TXT ;;
  }

  dimension: lane_num {
    type: number
    sql: ${TABLE}.LANE_NUM ;;
  }

  dimension: library_id {
    type: number
    sql: ${TABLE}.LIBRARY_ID ;;
  }

  dimension: max_speed_limit_num {
    type: number
    sql: ${TABLE}.MAX_SPEED_LIMIT_NUM ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: min_speed_limit_num {
    type: number
    sql: ${TABLE}.MIN_SPEED_LIMIT_NUM ;;
  }

  dimension: type_device_cd {
    type: number
    sql: ${TABLE}.TYPE_DEVICE_CD ;;
  }

  dimension: type_device_desc {
    type: string
    sql: ${TABLE}.TYPE_DEVICE_DESC ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_DESC ;;
  }

  dimension: type_lane_cd {
    type: number
    sql: ${TABLE}.TYPE_LANE_CD ;;
  }

  dimension: type_lane_desc {
    type: string
    sql: ${TABLE}.TYPE_LANE_DESC ;;
  }

  dimension: vsl_max_speed_limit_num {
    type: number
    sql: ${TABLE}.VSL_MAX_SPEED_LIMIT_NUM ;;
  }

  dimension: vsl_min_speed_limit_num {
    type: number
    sql: ${TABLE}.VSL_MIN_SPEED_LIMIT_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
