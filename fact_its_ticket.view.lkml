view: fact_its_ticket {
  sql_table_name: CTMR_MART.FACT_ITS_TICKET ;;

  dimension: canned_sk {
    type: number
    sql: ${TABLE}.CANNED_SK ;;
  }

  dimension: cctv_camera_sk {
    type: number
    sql: ${TABLE}.CCTV_CAMERA_SK ;;
  }

  dimension_group: closed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CLOSED ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATED ;;
  }

  dimension: ctms_device_sk {
    type: number
    sql: ${TABLE}.CTMS_DEVICE_SK ;;
  }

  dimension: dept_id {
    type: number
    sql: ${TABLE}.DEPT_ID ;;
  }

  dimension: dept_sk {
    type: number
    sql: ${TABLE}.DEPT_SK ;;
  }

  dimension: device_id {
    type: number
    sql: ${TABLE}.DEVICE_ID ;;
  }

  dimension: devicetype_id {
    type: number
    sql: ${TABLE}.DEVICETYPE_ID ;;
  }

  dimension: devicetype_name {
    type: string
    sql: ${TABLE}.DEVICETYPE_NAME ;;
  }

  dimension: devsapno {
    type: number
    sql: ${TABLE}.DEVSAPNO ;;
  }

  dimension_group: duedate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DUEDATE ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.EMAIL ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension: ip_address {
    type: string
    sql: ${TABLE}.IP_ADDRESS ;;
  }

  dimension: isanswered {
    type: number
    sql: ${TABLE}.ISANSWERED ;;
  }

  dimension: isoverdue {
    type: number
    sql: ${TABLE}.ISOVERDUE ;;
  }

  dimension: its_device_sk {
    type: number
    sql: ${TABLE}.ITS_DEVICE_SK ;;
  }

  dimension_group: lastmessage {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LASTMESSAGE ;;
  }

  dimension_group: lastresponse {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LASTRESPONSE ;;
  }

  dimension: lead_id {
    type: number
    sql: ${TABLE}.LEAD_ID ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.NAME ;;
  }

  dimension: part_id {
    type: string
    sql: ${TABLE}.PART_ID ;;
  }

  dimension: part_ids {
    type: string
    sql: ${TABLE}.PART_IDS ;;
  }

  dimension: phone {
    type: string
    sql: ${TABLE}.PHONE ;;
  }

  dimension: phone_ext {
    type: string
    sql: ${TABLE}.PHONE_EXT ;;
  }

  dimension: priority_id {
    type: number
    sql: ${TABLE}.PRIORITY_ID ;;
  }

  dimension: priority_sk {
    type: number
    sql: ${TABLE}.PRIORITY_SK ;;
  }

  dimension_group: reopened {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.REOPENED ;;
  }

  dimension: repairkit_id {
    type: string
    sql: ${TABLE}.REPAIRKIT_ID ;;
  }

  dimension: repairkit_ids {
    type: string
    sql: ${TABLE}.REPAIRKIT_IDS ;;
  }

  dimension: repairkit_sk {
    type: number
    sql: ${TABLE}.REPAIRKIT_SK ;;
  }

  dimension: respcode {
    type: string
    sql: ${TABLE}.RESPCODE ;;
  }

  dimension: sla_id {
    type: number
    sql: ${TABLE}.SLA_ID ;;
  }

  dimension: sla_name {
    type: string
    sql: ${TABLE}.SLA_NAME ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.SOURCE ;;
  }

  dimension: staff_id {
    type: number
    sql: ${TABLE}.STAFF_ID ;;
  }

  dimension: staff_sk {
    type: number
    sql: ${TABLE}.STAFF_SK ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.STATUS ;;
  }

  dimension: subject {
    type: string
    sql: ${TABLE}.SUBJECT ;;
  }

  dimension: team_id {
    type: number
    sql: ${TABLE}.TEAM_ID ;;
  }

  dimension: tech_id {
    type: number
    sql: ${TABLE}.TECH_ID ;;
  }

  dimension: tech_ids {
    type: string
    sql: ${TABLE}.TECH_IDS ;;
  }

  dimension: tech_sk {
    type: number
    sql: ${TABLE}.TECH_SK ;;
  }

  dimension: ticket_id {
    type: number
    sql: ${TABLE}.TICKET_ID ;;
  }

  dimension: ticket_sk {
    type: number
    sql: ${TABLE}.TICKET_SK ;;
  }

  dimension: ticketid {
    type: number
    value_format_name: id
    sql: ${TABLE}.TICKETID ;;
  }

  dimension: time_spent {
    type: number
    sql: ${TABLE}.TIME_SPENT ;;
  }

  dimension: tool_id {
    type: string
    sql: ${TABLE}.TOOL_ID ;;
  }

  dimension: tool_ids {
    type: string
    sql: ${TABLE}.TOOL_IDS ;;
  }

  dimension: topic_id {
    type: number
    sql: ${TABLE}.TOPIC_ID ;;
  }

  dimension: trailer_id {
    type: number
    sql: ${TABLE}.TRAILER_ID ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.UPDATED ;;
  }

  dimension: vehicle_id {
    type: number
    sql: ${TABLE}.VEHICLE_ID ;;
  }

  dimension: vehicle_ids {
    type: string
    sql: ${TABLE}.VEHICLE_IDS ;;
  }

  dimension: vehicle_sk {
    type: number
    sql: ${TABLE}.VEHICLE_SK ;;
  }

  measure: count {
    type: count
    drill_fields: [name, sla_name, devicetype_name]
  }
}
