view: fact_vsl_proposed_speed {
  sql_table_name: CTMR_MART.FACT_VSL_PROPOSED_SPEED ;;

  dimension: can_post_flg {
    type: number
    sql: ${TABLE}.CAN_POST_FLG ;;
  }

  dimension_group: clear_issued_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CLEAR_ISSUED_DT ;;
  }

  dimension: clear_issued_dt_sk {
    type: number
    sql: ${TABLE}.CLEAR_ISSUED_DT_SK ;;
  }

  dimension: clear_issued_tm_sk {
    type: number
    sql: ${TABLE}.CLEAR_ISSUED_TM_SK ;;
  }

  dimension: corridor_device_sk {
    type: number
    sql: ${TABLE}.CORRIDOR_DEVICE_SK ;;
  }

  dimension: corridor_sk {
    type: number
    sql: ${TABLE}.CORRIDOR_SK ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension_group: cycle_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CYCLE_DT ;;
  }

  dimension: cycle_dt_sk {
    type: number
    sql: ${TABLE}.CYCLE_DT_SK ;;
  }

  dimension: cycle_num {
    type: number
    sql: ${TABLE}.CYCLE_NUM ;;
  }

  dimension: cycle_tm_sk {
    type: number
    sql: ${TABLE}.CYCLE_TM_SK ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension: gantry_device_sk {
    type: number
    sql: ${TABLE}.GANTRY_DEVICE_SK ;;
  }

  dimension: gantry_sk {
    type: number
    sql: ${TABLE}.GANTRY_SK ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: reason_txt {
    type: string
    sql: ${TABLE}.REASON_TXT ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: speed_proposed_flt {
    type: number
    sql: ${TABLE}.SPEED_PROPOSED_FLT ;;
  }

  dimension: type_direction_sk {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_SK ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  dimension: vsl_proposed_speed_id {
    type: number
    sql: ${TABLE}.VSL_PROPOSED_SPEED_ID ;;
  }

  dimension: vsl_proposed_speed_sk {
    type: number
    sql: ${TABLE}.VSL_PROPOSED_SPEED_SK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
