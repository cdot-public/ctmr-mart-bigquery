view: dim_type_call_log_agg {
  sql_table_name: CTMR_MART.DIM_TYPE_CALL_LOG_AGG ;;

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: num_of_type_call_log {
    type: number
    sql: ${TABLE}.NUM_OF_TYPE_CALL_LOG ;;
  }

  dimension: type_call_log_agg_sk {
    type: number
    sql: ${TABLE}.TYPE_CALL_LOG_AGG_SK ;;
  }

  dimension: type_call_log_desc {
    type: string
    sql: ${TABLE}.TYPE_CALL_LOG_DESC ;;
  }

  dimension: type_call_log_id {
    type: string
    sql: ${TABLE}.TYPE_CALL_LOG_ID ;;
  }

  dimension: type_call_log_rnk {
    type: number
    sql: ${TABLE}.TYPE_CALL_LOG_RNK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
