view: dim_segment {
  sql_table_name: CTMR_MART.DIM_SEGMENT ;;

  dimension: back_day_num {
    type: number
    sql: ${TABLE}.BACK_DAY_NUM ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_begin {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_BEGIN_DATE ;;
  }

  dimension_group: dim_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_END_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension: end_node_id {
    type: number
    sql: ${TABLE}.END_NODE_ID ;;
  }

  dimension: expected_travel_time {
    type: number
    sql: ${TABLE}.EXPECTED_TRAVEL_TIME ;;
  }

  dimension: free_flow_travel_time {
    type: number
    sql: ${TABLE}.FREE_FLOW_TRAVEL_TIME ;;
  }

  dimension: group_id {
    type: number
    sql: ${TABLE}.GROUP_ID ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: mm_end_flt {
    type: number
    sql: ${TABLE}.MM_END_FLT ;;
  }

  dimension: mm_start_flt {
    type: number
    sql: ${TABLE}.MM_START_FLT ;;
  }

  dimension: occupancy_threshold_flt {
    type: number
    sql: ${TABLE}.OCCUPANCY_THRESHOLD_FLT ;;
  }

  dimension: publish_to_web_flg {
    type: number
    sql: ${TABLE}.PUBLISH_TO_WEB_FLG ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: segment_common_name_txt {
    type: string
    sql: ${TABLE}.SEGMENT_COMMON_NAME_TXT ;;
  }

  dimension: segment_id {
    type: number
    sql: ${TABLE}.SEGMENT_ID ;;
  }

  dimension: segment_length_miles_flt {
    type: number
    sql: ${TABLE}.SEGMENT_LENGTH_MILES_FLT ;;
  }

  dimension: segment_sk {
    type: number
    sql: ${TABLE}.SEGMENT_SK ;;
  }

  dimension: sort_order_num {
    type: number
    sql: ${TABLE}.SORT_ORDER_NUM ;;
  }

  dimension: speed_limit_num {
    type: number
    sql: ${TABLE}.SPEED_LIMIT_NUM ;;
  }

  dimension: start_node_id {
    type: number
    sql: ${TABLE}.START_NODE_ID ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_code_value {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_CODE_VALUE ;;
  }

  dimension: type_direction_long_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_LONG_DESC ;;
  }

  dimension: type_direction_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_SHORT_DESC ;;
  }

  dimension: type_direction_sort_order {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_SORT_ORDER ;;
  }

  dimension: type_reason_cd {
    type: number
    sql: ${TABLE}.TYPE_REASON_CD ;;
  }

  dimension: type_reason_code_value {
    type: string
    sql: ${TABLE}.TYPE_REASON_CODE_VALUE ;;
  }

  dimension: type_reason_long_desc {
    type: string
    sql: ${TABLE}.TYPE_REASON_LONG_DESC ;;
  }

  dimension: type_reason_short_desc {
    type: string
    sql: ${TABLE}.TYPE_REASON_SHORT_DESC ;;
  }

  dimension: type_reason_sort_order {
    type: number
    sql: ${TABLE}.TYPE_REASON_SORT_ORDER ;;
  }

  dimension: type_road_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_CD ;;
  }

  dimension: type_road_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC ;;
  }

  dimension: type_road_short_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_SHORT_DESC ;;
  }

  dimension: type_segment_cd {
    type: number
    sql: ${TABLE}.TYPE_SEGMENT_CD ;;
  }

  dimension: type_segment_code_value {
    type: string
    sql: ${TABLE}.TYPE_SEGMENT_CODE_VALUE ;;
  }

  dimension: type_segment_long_desc {
    type: string
    sql: ${TABLE}.TYPE_SEGMENT_LONG_DESC ;;
  }

  dimension: type_segment_short_desc {
    type: string
    sql: ${TABLE}.TYPE_SEGMENT_SHORT_DESC ;;
  }

  dimension: type_segment_sort_order {
    type: number
    sql: ${TABLE}.TYPE_SEGMENT_SORT_ORDER ;;
  }

  dimension: type_segment_status_cd {
    type: number
    sql: ${TABLE}.TYPE_SEGMENT_STATUS_CD ;;
  }

  dimension: type_segment_status_long {
    type: string
    sql: ${TABLE}.TYPE_SEGMENT_STATUS_LONG ;;
  }

  dimension: type_segment_status_short {
    type: string
    sql: ${TABLE}.TYPE_SEGMENT_STATUS_SHORT ;;
  }

  dimension: type_segment_status_sort {
    type: number
    sql: ${TABLE}.TYPE_SEGMENT_STATUS_SORT ;;
  }

  dimension: type_segment_status_value {
    type: string
    sql: ${TABLE}.TYPE_SEGMENT_STATUS_VALUE ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: working_day_flg {
    type: number
    sql: ${TABLE}.WORKING_DAY_FLG ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
