view: fact_atr_daily_axle_bin_data {
  sql_table_name: CTMR_MART.FACT_ATR_DAILY_AXLE_BIN_DATA ;;

  dimension: atr_daily_axle_bin_data_id {
    type: number
    sql: ${TABLE}.ATR_DAILY_AXLE_BIN_DATA_ID ;;
  }

  dimension: atr_daily_axle_bin_data_sk {
    type: number
    sql: ${TABLE}.ATR_DAILY_AXLE_BIN_DATA_SK ;;
  }

  dimension: atr_daily_file_info_id {
    type: number
    sql: ${TABLE}.ATR_DAILY_FILE_INFO_ID ;;
  }

  dimension: atr_daily_sk {
    type: number
    sql: ${TABLE}.ATR_DAILY_SK ;;
  }

  dimension: axle_bin_count_num {
    type: number
    sql: ${TABLE}.AXLE_BIN_COUNT_NUM ;;
  }

  dimension: axle_bin_num {
    type: number
    sql: ${TABLE}.AXLE_BIN_NUM ;;
  }

  dimension_group: collection_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.COLLECTION_DT ;;
  }

  dimension: collection_dt_sk {
    type: number
    sql: ${TABLE}.COLLECTION_DT_SK ;;
  }

  dimension: collection_tm_sk {
    type: number
    sql: ${TABLE}.COLLECTION_TM_SK ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension: interval_num {
    type: number
    sql: ${TABLE}.INTERVAL_NUM ;;
  }

  dimension: lane_desc_txt {
    type: string
    sql: ${TABLE}.LANE_DESC_TXT ;;
  }

  dimension: lane_num {
    type: number
    sql: ${TABLE}.LANE_NUM ;;
  }

  dimension: lane_sk {
    type: number
    sql: ${TABLE}.LANE_SK ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: stand_lane_num {
    type: number
    sql: ${TABLE}.STAND_LANE_NUM ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
