view: dim_rtms_lane2 {
  sql_table_name: CTMR_MART.DIM_RTMS_LANE2 ;;

  dimension: device_direction_cd {
    type: number
    sql: ${TABLE}.DEVICE_DIRECTION_CD ;;
  }

  dimension: device_direction_desc {
    type: string
    sql: ${TABLE}.DEVICE_DIRECTION_DESC ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_begin {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_BEGIN_DATE ;;
  }

  dimension_group: dim_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_END_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension: lane_desc_txt {
    type: string
    sql: ${TABLE}.LANE_DESC_TXT ;;
  }

  dimension: lane_num {
    type: number
    sql: ${TABLE}.LANE_NUM ;;
  }

  dimension: lane_sk {
    type: number
    sql: ${TABLE}.LANE_SK ;;
  }

  dimension: new_rtms_lane_sk {
    type: number
    sql: ${TABLE}.NEW_RTMS_LANE_SK ;;
  }

  dimension: rtms_lane_sk {
    type: number
    sql: ${TABLE}.RTMS_LANE_SK ;;
  }

  dimension: rtms_sk {
    type: number
    sql: ${TABLE}.RTMS_SK ;;
  }

  dimension: segment_common_name_txt {
    type: string
    sql: ${TABLE}.SEGMENT_COMMON_NAME_TXT ;;
  }

  dimension: segment_direction_cd {
    type: number
    sql: ${TABLE}.SEGMENT_DIRECTION_CD ;;
  }

  dimension: segment_direction_desc {
    type: string
    sql: ${TABLE}.SEGMENT_DIRECTION_DESC ;;
  }

  dimension: segment_sk {
    type: number
    sql: ${TABLE}.SEGMENT_SK ;;
  }

  dimension: stand_lane_num {
    type: number
    sql: ${TABLE}.STAND_LANE_NUM ;;
  }

  dimension: type_lane_cd {
    type: number
    sql: ${TABLE}.TYPE_LANE_CD ;;
  }

  dimension: type_lane_short_desc {
    type: string
    sql: ${TABLE}.TYPE_LANE_SHORT_DESC ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
