view: dim_route_weather_route {
  sql_table_name: CTMR_MART.DIM_ROUTE_WEATHER_ROUTE ;;

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_begin {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_BEGIN_DATE ;;
  }

  dimension_group: dim_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_END_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension: route_end_mm {
    type: number
    sql: ${TABLE}.ROUTE_END_MM ;;
  }

  dimension: route_end_road_id {
    type: number
    sql: ${TABLE}.ROUTE_END_ROAD_ID ;;
  }

  dimension: route_end_road_sk {
    type: number
    sql: ${TABLE}.ROUTE_END_ROAD_SK ;;
  }

  dimension: route_id {
    type: number
    sql: ${TABLE}.ROUTE_ID ;;
  }

  dimension: route_length_miles_flt {
    type: number
    sql: ${TABLE}.ROUTE_LENGTH_MILES_FLT ;;
  }

  dimension: route_name_txt {
    type: string
    sql: ${TABLE}.ROUTE_NAME_TXT ;;
  }

  dimension: route_sk {
    type: number
    sql: ${TABLE}.ROUTE_SK ;;
  }

  dimension: route_start_mm {
    type: number
    sql: ${TABLE}.ROUTE_START_MM ;;
  }

  dimension: route_start_road_id {
    type: number
    sql: ${TABLE}.ROUTE_START_ROAD_ID ;;
  }

  dimension: route_start_road_sk {
    type: number
    sql: ${TABLE}.ROUTE_START_ROAD_SK ;;
  }

  dimension: route_wr_sk {
    type: number
    sql: ${TABLE}.ROUTE_WR_SK ;;
  }

  dimension: weather_route_end_mm {
    type: number
    sql: ${TABLE}.WEATHER_ROUTE_END_MM ;;
  }

  dimension: weather_route_id {
    type: number
    sql: ${TABLE}.WEATHER_ROUTE_ID ;;
  }

  dimension: weather_route_name {
    type: string
    sql: ${TABLE}.WEATHER_ROUTE_NAME ;;
  }

  dimension: weather_route_sk {
    type: number
    sql: ${TABLE}.WEATHER_ROUTE_SK ;;
  }

  dimension: weather_route_start_mm {
    type: number
    sql: ${TABLE}.WEATHER_ROUTE_START_MM ;;
  }

  measure: count {
    type: count
    drill_fields: [weather_route_name]
  }
}
