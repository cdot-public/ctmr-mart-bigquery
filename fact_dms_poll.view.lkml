view: fact_dms_poll {
  sql_table_name: CTMR_MART.FACT_DMS_POLL ;;

  dimension: alt_message_txt {
    type: string
    sql: ${TABLE}.ALT_MESSAGE_TXT ;;
  }

  dimension: attached_device_error_flg {
    type: number
    sql: ${TABLE}.ATTACHED_DEVICE_ERROR_FLG ;;
  }

  dimension: beacons_flg {
    type: number
    sql: ${TABLE}.BEACONS_FLG ;;
  }

  dimension: brightness_level_num {
    type: number
    sql: ${TABLE}.BRIGHTNESS_LEVEL_NUM ;;
  }

  dimension: climate_error_flg {
    type: number
    sql: ${TABLE}.CLIMATE_ERROR_FLG ;;
  }

  dimension: comm_error_flg {
    type: number
    sql: ${TABLE}.COMM_ERROR_FLG ;;
  }

  dimension: controller_error_flg {
    type: number
    sql: ${TABLE}.CONTROLLER_ERROR_FLG ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_dt_sk {
    type: number
    sql: ${TABLE}.CREATE_DT_SK ;;
  }

  dimension: create_tm_sk {
    type: number
    sql: ${TABLE}.CREATE_TM_SK ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: current_message_owner_txt {
    type: string
    sql: ${TABLE}.CURRENT_MESSAGE_OWNER_TXT ;;
  }

  dimension: device_online_flg {
    type: number
    sql: ${TABLE}.DEVICE_ONLINE_FLG ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dms_poll_id {
    type: number
    sql: ${TABLE}.DMS_POLL_ID ;;
  }

  dimension: dms_poll_sk {
    type: number
    sql: ${TABLE}.DMS_POLL_SK ;;
  }

  dimension: dms_sk {
    type: number
    sql: ${TABLE}.DMS_SK ;;
  }

  dimension: engine_rpm_parameter_num {
    type: number
    sql: ${TABLE}.ENGINE_RPM_PARAMETER_NUM ;;
  }

  dimension: event_sk {
    type: number
    sql: ${TABLE}.EVENT_SK ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension: file_binary_id {
    type: number
    sql: ${TABLE}.FILE_BINARY_ID ;;
  }

  dimension: fuel_level_num {
    type: number
    sql: ${TABLE}.FUEL_LEVEL_NUM ;;
  }

  dimension: has_graphics_flg {
    type: number
    sql: ${TABLE}.HAS_GRAPHICS_FLG ;;
  }

  dimension: lamp_error_flg {
    type: number
    sql: ${TABLE}.LAMP_ERROR_FLG ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: line_volts_num {
    type: number
    sql: ${TABLE}.LINE_VOLTS_NUM ;;
  }

  dimension: low_fuel_threshold_num {
    type: number
    sql: ${TABLE}.LOW_FUEL_THRESHOLD_NUM ;;
  }

  dimension: max_ambient_temp_flt {
    type: number
    sql: ${TABLE}.MAX_AMBIENT_TEMP_FLT ;;
  }

  dimension: max_sign_temp_flt {
    type: number
    sql: ${TABLE}.MAX_SIGN_TEMP_FLT ;;
  }

  dimension: msg_error_flg {
    type: number
    sql: ${TABLE}.MSG_ERROR_FLG ;;
  }

  dimension: multi_syntax_err_pos_num {
    type: number
    sql: ${TABLE}.MULTI_SYNTAX_ERR_POS_NUM ;;
  }

  dimension: num_lamp_stuck_off_num {
    type: number
    sql: ${TABLE}.NUM_LAMP_STUCK_OFF_NUM ;;
  }

  dimension: num_lamp_stuck_on_num {
    type: number
    sql: ${TABLE}.NUM_LAMP_STUCK_ON_NUM ;;
  }

  dimension: num_pixel_test_errors_num {
    type: number
    sql: ${TABLE}.NUM_PIXEL_TEST_ERRORS_NUM ;;
  }

  dimension: open_door_error_flg {
    type: number
    sql: ${TABLE}.OPEN_DOOR_ERROR_FLG ;;
  }

  dimension: owner_txt {
    type: string
    sql: ${TABLE}.OWNER_TXT ;;
  }

  dimension: photocell_error_flg {
    type: number
    sql: ${TABLE}.PHOTOCELL_ERROR_FLG ;;
  }

  dimension: pixel_error_flg {
    type: number
    sql: ${TABLE}.PIXEL_ERROR_FLG ;;
  }

  dimension_group: pixel_test_time_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.PIXEL_TEST_TIME_DT ;;
  }

  dimension: poll_message_txt {
    type: string
    sql: ${TABLE}.POLL_MESSAGE_TXT ;;
  }

  dimension: poll_status_flg {
    type: number
    sql: ${TABLE}.POLL_STATUS_FLG ;;
  }

  dimension: power_error_flg {
    type: number
    sql: ${TABLE}.POWER_ERROR_FLG ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: sign_voltage_num {
    type: number
    sql: ${TABLE}.SIGN_VOLTAGE_NUM ;;
  }

  dimension: speed_msg_flg {
    type: number
    sql: ${TABLE}.SPEED_MSG_FLG ;;
  }

  dimension: temp_error_flg {
    type: number
    sql: ${TABLE}.TEMP_ERROR_FLG ;;
  }

  dimension: toll_msg_flg {
    type: number
    sql: ${TABLE}.TOLL_MSG_FLG ;;
  }

  dimension: type_dms_controller_cd {
    type: number
    sql: ${TABLE}.TYPE_DMS_CONTROLLER_CD ;;
  }

  dimension: type_lus_message_sk {
    type: number
    sql: ${TABLE}.TYPE_LUS_MESSAGE_SK ;;
  }

  dimension: type_message_source_cd {
    type: number
    sql: ${TABLE}.TYPE_MESSAGE_SOURCE_CD ;;
  }

  dimension: type_multi_syntax_error_cd {
    type: number
    sql: ${TABLE}.TYPE_MULTI_SYNTAX_ERROR_CD ;;
  }

  dimension: type_power_source_cd {
    type: number
    sql: ${TABLE}.TYPE_POWER_SOURCE_CD ;;
  }

  dimension: type_priority_message_cd {
    type: number
    sql: ${TABLE}.TYPE_PRIORITY_MESSAGE_CD ;;
  }

  dimension: type_priority_message_sk {
    type: number
    sql: ${TABLE}.TYPE_PRIORITY_MESSAGE_SK ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
