view: dim_crossroad {
  sql_table_name: CTMR_MART.DIM_CROSSROAD ;;

  dimension: crossroad_desc_txt {
    type: string
    sql: ${TABLE}.CROSSROAD_DESC_TXT ;;
  }

  dimension: crossroad_id {
    type: number
    sql: ${TABLE}.CROSSROAD_ID ;;
  }

  dimension: crossroad_sk {
    type: number
    sql: ${TABLE}.CROSSROAD_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: type_agency_dispatch_cd {
    type: number
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_CD ;;
  }

  dimension: type_agency_dispatch_long {
    type: string
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_LONG ;;
  }

  dimension: type_agency_dispatch_short {
    type: string
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_SHORT ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
