view: fact_agency_dispatch {
  sql_table_name: CTMR_MART.FACT_AGENCY_DISPATCH ;;

  dimension: agency_dispatch_id {
    type: number
    sql: ${TABLE}.AGENCY_DISPATCH_ID ;;
  }

  dimension: agency_dispatch_sk {
    type: number
    sql: ${TABLE}.AGENCY_DISPATCH_SK ;;
  }

  dimension: agency_lane_agg_sk {
    type: number
    sql: ${TABLE}.AGENCY_LANE_AGG_SK ;;
  }

  dimension: agency_responder_sk {
    type: number
    sql: ${TABLE}.AGENCY_RESPONDER_SK ;;
  }

  dimension: agency_services_agg_sk {
    type: number
    sql: ${TABLE}.AGENCY_SERVICES_AGG_SK ;;
  }

  dimension_group: arrival_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ARRIVAL_DT ;;
  }

  dimension: arrival_dt_sk {
    type: number
    sql: ${TABLE}.ARRIVAL_DT_SK ;;
  }

  dimension: arrival_tm_sk {
    type: number
    sql: ${TABLE}.ARRIVAL_TM_SK ;;
  }

  dimension_group: clear_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CLEAR_DT ;;
  }

  dimension: clear_dt_sk {
    type: number
    sql: ${TABLE}.CLEAR_DT_SK ;;
  }

  dimension: clear_tm_sk {
    type: number
    sql: ${TABLE}.CLEAR_TM_SK ;;
  }

  dimension: cmv_company_txt {
    type: string
    sql: ${TABLE}.CMV_COMPANY_TXT ;;
  }

  dimension: cmv_hazmat_flg {
    type: number
    sql: ${TABLE}.CMV_HAZMAT_FLG ;;
  }

  dimension: comment_txt {
    type: string
    sql: ${TABLE}.COMMENT_TXT ;;
  }

  dimension: crossroad_sk {
    type: number
    sql: ${TABLE}.CROSSROAD_SK ;;
  }

  dimension_group: departure {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DEPARTURE_TIME ;;
  }

  dimension_group: dispatch_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DISPATCH_DT ;;
  }

  dimension: dispatch_dt_sk {
    type: number
    sql: ${TABLE}.DISPATCH_DT_SK ;;
  }

  dimension: dispatch_tm_sk {
    type: number
    sql: ${TABLE}.DISPATCH_TM_SK ;;
  }

  dimension: driver_recvd_forms_flg {
    type: number
    sql: ${TABLE}.DRIVER_RECVD_FORMS_FLG ;;
  }

  dimension: driver_refused_svc_flg {
    type: number
    sql: ${TABLE}.DRIVER_REFUSED_SVC_FLG ;;
  }

  dimension: escape_ramp_used_flg {
    type: number
    sql: ${TABLE}.ESCAPE_RAMP_USED_FLG ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension: injury_flg {
    type: number
    sql: ${TABLE}.INJURY_FLG ;;
  }

  dimension: law_enf_notify_flg {
    type: number
    sql: ${TABLE}.LAW_ENF_NOTIFY_FLG ;;
  }

  dimension: license_plate_txt {
    type: string
    sql: ${TABLE}.LICENSE_PLATE_TXT ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: service_provided_flg {
    type: number
    sql: ${TABLE}.SERVICE_PROVIDED_FLG ;;
  }

  dimension: traction_device_applied_flg {
    type: number
    sql: ${TABLE}.TRACTION_DEVICE_APPLIED_FLG ;;
  }

  dimension: traction_device_avail_flg {
    type: number
    sql: ${TABLE}.TRACTION_DEVICE_AVAIL_FLG ;;
  }

  dimension: tread_depth_flg {
    type: number
    sql: ${TABLE}.TREAD_DEPTH_FLG ;;
  }

  dimension: type_agency_dispatch_incid_sk {
    type: number
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_INCID_SK ;;
  }

  dimension: type_agency_dispatch_sk {
    type: number
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_SK ;;
  }

  dimension: type_agency_dispatch_srce_sk {
    type: number
    sql: ${TABLE}.TYPE_AGENCY_DISPATCH_SRCE_SK ;;
  }

  dimension: type_agency_no_service_sk {
    type: number
    sql: ${TABLE}.TYPE_AGENCY_NO_SERVICE_SK ;;
  }

  dimension: type_chain_law_sk {
    type: number
    sql: ${TABLE}.TYPE_CHAIN_LAW_SK ;;
  }

  dimension: type_cmv_sk {
    type: number
    sql: ${TABLE}.TYPE_CMV_SK ;;
  }

  dimension: type_cmv_state_sk {
    type: number
    sql: ${TABLE}.TYPE_CMV_STATE_SK ;;
  }

  dimension: type_direction_sk {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_SK ;;
  }

  dimension: type_escape_ramp_loc_sk {
    type: number
    sql: ${TABLE}.TYPE_ESCAPE_RAMP_LOC_SK ;;
  }

  dimension: type_roadway_closure_sk {
    type: number
    sql: ${TABLE}.TYPE_ROADWAY_CLOSURE_SK ;;
  }

  dimension: type_vehicle_sk {
    type: number
    sql: ${TABLE}.TYPE_VEHICLE_SK ;;
  }

  dimension: usdot_txt {
    type: string
    sql: ${TABLE}.USDOT_TXT ;;
  }

  dimension: vehicle_drivable_flg {
    type: number
    sql: ${TABLE}.VEHICLE_DRIVABLE_FLG ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  dimension: weather_related_dispatch_flg {
    type: number
    sql: ${TABLE}.WEATHER_RELATED_DISPATCH_FLG ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
