view: dim_avi_pair {
  sql_table_name: CTMR_MART.DIM_AVI_PAIR ;;

  dimension: avi_dest_mm {
    type: number
    sql: ${TABLE}.AVI_DEST_MM ;;
  }

  dimension: avi_dest_name {
    type: string
    sql: ${TABLE}.AVI_DEST_NAME ;;
  }

  dimension: avi_dest_sk {
    type: number
    sql: ${TABLE}.AVI_DEST_SK ;;
  }

  dimension: avi_pair_id {
    type: number
    sql: ${TABLE}.AVI_PAIR_ID ;;
  }

  dimension: avi_pair_sk {
    type: number
    sql: ${TABLE}.AVI_PAIR_SK ;;
  }

  dimension: avi_source_mm {
    type: number
    sql: ${TABLE}.AVI_SOURCE_MM ;;
  }

  dimension: avi_source_name {
    type: string
    sql: ${TABLE}.AVI_SOURCE_NAME ;;
  }

  dimension: avi_source_sk {
    type: number
    sql: ${TABLE}.AVI_SOURCE_SK ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: distance_flt {
    type: number
    sql: ${TABLE}.DISTANCE_FLT ;;
  }

  dimension: expected_tt_millisec_num {
    type: number
    sql: ${TABLE}.EXPECTED_TT_MILLISEC_NUM ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  measure: count {
    type: count
    drill_fields: [avi_source_name, avi_dest_name]
  }
}
