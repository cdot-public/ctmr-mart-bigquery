view: dim_route_device {
  sql_table_name: CTMR_MART.DIM_ROUTE_DEVICE ;;

  dimension: device_id {
    type: number
    sql: ${TABLE}.DEVICE_ID ;;
  }

  dimension: device_mm {
    type: number
    sql: ${TABLE}.DEVICE_MM ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_begin {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_BEGIN_DATE ;;
  }

  dimension_group: dim_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_END_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension: route_device_sk {
    type: number
    sql: ${TABLE}.ROUTE_DEVICE_SK ;;
  }

  dimension: route_end_mm {
    type: number
    sql: ${TABLE}.ROUTE_END_MM ;;
  }

  dimension: route_end_road_id {
    type: number
    sql: ${TABLE}.ROUTE_END_ROAD_ID ;;
  }

  dimension: route_end_road_sk {
    type: number
    sql: ${TABLE}.ROUTE_END_ROAD_SK ;;
  }

  dimension: route_id {
    type: number
    sql: ${TABLE}.ROUTE_ID ;;
  }

  dimension: route_length_miles_flt {
    type: number
    sql: ${TABLE}.ROUTE_LENGTH_MILES_FLT ;;
  }

  dimension: route_name_txt {
    type: string
    sql: ${TABLE}.ROUTE_NAME_TXT ;;
  }

  dimension: route_sk {
    type: number
    sql: ${TABLE}.ROUTE_SK ;;
  }

  dimension: route_start_mm {
    type: number
    sql: ${TABLE}.ROUTE_START_MM ;;
  }

  dimension: route_start_road_id {
    type: number
    sql: ${TABLE}.ROUTE_START_ROAD_ID ;;
  }

  dimension: route_start_road_sk {
    type: number
    sql: ${TABLE}.ROUTE_START_ROAD_SK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
