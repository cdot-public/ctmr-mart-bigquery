view: fact_call_log {
  sql_table_name: CTMR_MART.FACT_CALL_LOG ;;

  dimension_group: call_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CALL_DT ;;
  }

  dimension: call_dt_sk {
    type: number
    sql: ${TABLE}.CALL_DT_SK ;;
  }

  dimension: call_in_txt {
    type: string
    sql: ${TABLE}.CALL_IN_TXT ;;
  }

  dimension: call_log_id {
    type: number
    sql: ${TABLE}.CALL_LOG_ID ;;
  }

  dimension: call_log_sk {
    type: number
    sql: ${TABLE}.CALL_LOG_SK ;;
  }

  dimension: call_tm_sk {
    type: number
    sql: ${TABLE}.CALL_TM_SK ;;
  }

  dimension: comments_txt {
    type: string
    sql: ${TABLE}.COMMENTS_TXT ;;
  }

  dimension: completed_flg {
    type: number
    sql: ${TABLE}.COMPLETED_FLG ;;
  }

  dimension: contact_email_txt {
    type: string
    sql: ${TABLE}.CONTACT_EMAIL_TXT ;;
  }

  dimension: contact_name_txt {
    type: string
    sql: ${TABLE}.CONTACT_NAME_TXT ;;
  }

  dimension: contact_phone_txt {
    type: string
    sql: ${TABLE}.CONTACT_PHONE_TXT ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: event_sk {
    type: number
    sql: ${TABLE}.EVENT_SK ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension: isboth_direction_flg {
    type: number
    sql: ${TABLE}.ISBOTH_DIRECTION_FLG ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: location_desc_txt {
    type: string
    sql: ${TABLE}.LOCATION_DESC_TXT ;;
  }

  dimension: mm_end_flt {
    type: number
    sql: ${TABLE}.MM_END_FLT ;;
  }

  dimension: mm_start_flt {
    type: number
    sql: ${TABLE}.MM_START_FLT ;;
  }

  dimension: on_call_txt {
    type: string
    sql: ${TABLE}.ON_CALL_TXT ;;
  }

  dimension: report_time_txt {
    type: string
    sql: ${TABLE}.REPORT_TIME_TXT ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: roadway_name_txt {
    type: string
    sql: ${TABLE}.ROADWAY_NAME_TXT ;;
  }

  dimension: security_group_id {
    type: number
    sql: ${TABLE}.SECURITY_GROUP_ID ;;
  }

  dimension: security_group_name {
    type: string
    sql: ${TABLE}.SECURITY_GROUP_NAME ;;
  }

  dimension: type_call_log_agency_cd {
    type: number
    sql: ${TABLE}.TYPE_CALL_LOG_AGENCY_CD ;;
  }

  dimension: type_call_log_agency_long {
    type: string
    sql: ${TABLE}.TYPE_CALL_LOG_AGENCY_LONG ;;
  }

  dimension: type_call_log_agency_short {
    type: string
    sql: ${TABLE}.TYPE_CALL_LOG_AGENCY_SHORT ;;
  }

  dimension: type_call_log_agency_sort {
    type: number
    sql: ${TABLE}.TYPE_CALL_LOG_AGENCY_SORT ;;
  }

  dimension: type_call_log_agency_value {
    type: string
    sql: ${TABLE}.TYPE_CALL_LOG_AGENCY_VALUE ;;
  }

  dimension: type_call_log_agg_sk {
    type: number
    sql: ${TABLE}.TYPE_CALL_LOG_AGG_SK ;;
  }

  dimension: type_call_log_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_CALL_LOG_DIRECTION_CD ;;
  }

  dimension: type_call_log_direction_long {
    type: string
    sql: ${TABLE}.TYPE_CALL_LOG_DIRECTION_LONG ;;
  }

  dimension: type_call_log_direction_short {
    type: string
    sql: ${TABLE}.TYPE_CALL_LOG_DIRECTION_SHORT ;;
  }

  dimension: type_call_log_direction_sort {
    type: number
    sql: ${TABLE}.TYPE_CALL_LOG_DIRECTION_SORT ;;
  }

  dimension: type_call_log_direction_value {
    type: string
    sql: ${TABLE}.TYPE_CALL_LOG_DIRECTION_VALUE ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_sign_message_cd {
    type: number
    sql: ${TABLE}.TYPE_SIGN_MESSAGE_CD ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: [security_group_name]
  }
}
