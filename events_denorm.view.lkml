view: events_denorm {
  sql_table_name: CTMR_MART.events_denorm ;;

  dimension: crossroad_desc_txt {
    type: string
    sql: ${TABLE}.CROSSROAD_DESC_TXT ;;
  }

  dimension_group: end_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.END_DT ;;
  }

  dimension: est_time_to_clear_txt {
    type: string
    sql: ${TABLE}.EST_TIME_TO_CLEAR_TXT ;;
  }

  dimension: event_id {
    type: number
    sql: ${TABLE}.EVENT_ID ;;
  }

  dimension: event_sub_type_headline {
    type: string
    sql: ${TABLE}.EVENT_SUB_TYPE_HEADLINE ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: road_id {
    type:number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: fatality_flg {
    type: number
    sql: ${TABLE}.FATALITY_FLG ;;
  }

  dimension: fatality_num {
    type: number
    sql: ${TABLE}.FATALITY_NUM ;;
  }

  dimension: latitude_flt {
    type: number
    sql: ${TABLE}.LATITUDE_FLT ;;
  }

  dimension: location_desc_txt {
    type: string
    sql: ${TABLE}.LOCATION_DESC_TXT ;;
  }

  dimension: longitude_flt {
    type: number
    sql: ${TABLE}.LONGITUDE_FLT ;;
  }

  dimension: major_event_flg {
    type: number
    sql: ${TABLE}.MAJOR_EVENT_FLG ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: mm_end_flt {
    type: number
    sql: ${TABLE}.MM_END_FLT ;;
  }

  dimension: mm_start_flt {
    type: number
    sql: ${TABLE}.MM_START_FLT ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension_group: start_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.START_DT ;;
  }

  dimension: type_direction_long_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_LONG_DESC ;;
  }

  dimension: type_event_category_short {
    type: string
    sql: ${TABLE}.TYPE_EVENT_CATEGORY_SHORT ;;
  }

  dimension: type_event_long {
    type: string
    sql: ${TABLE}.TYPE_EVENT_LONG ;;
  }

  dimension: type_event_severity_short {
    type: string
    sql: ${TABLE}.TYPE_EVENT_SEVERITY_SHORT ;;
  }

  dimension: type_event_status_short {
    type: string
    sql: ${TABLE}.TYPE_EVENT_STATUS_SHORT ;;
  }

  dimension: location {
    type: location
    sql_latitude: ${latitude_flt} ;;
    sql_longitude: ${longitude_flt} ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
  measure: severity {
    type: median
    sql: ${TABLE}.TYPE_EVENT_SEVERITY_CD ;;
  }
}
