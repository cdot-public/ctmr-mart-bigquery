view: fact_route_history {
  sql_table_name: CTMR_MART.FACT_ROUTE_HISTORY ;;

  dimension: avg_speed_flt {
    type: number
    sql: ${TABLE}.AVG_SPEED_FLT ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension_group: last_calculated_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_CALCULATED_DT ;;
  }

  dimension: last_calculated_dt_sk {
    type: number
    sql: ${TABLE}.LAST_CALCULATED_DT_SK ;;
  }

  dimension: last_calculated_tm_sk {
    type: number
    sql: ${TABLE}.LAST_CALCULATED_TM_SK ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: route_history_id {
    type: number
    sql: ${TABLE}.ROUTE_HISTORY_ID ;;
  }

  dimension: route_history_sk {
    type: number
    sql: ${TABLE}.ROUTE_HISTORY_SK ;;
  }

  dimension: route_sk {
    type: number
    sql: ${TABLE}.ROUTE_SK ;;
  }

  dimension: travel_time_num {
    type: number
    sql: ${TABLE}.TRAVEL_TIME_NUM ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: valid_reading_flag {
    type: number
    sql: ${TABLE}.VALID_READING_FLAG ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
