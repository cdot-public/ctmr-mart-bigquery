view: dim_corridor {
  sql_table_name: CTMR_MART.DIM_CORRIDOR ;;

  dimension: corridor_desc_txt {
    type: string
    sql: ${TABLE}.CORRIDOR_DESC_TXT ;;
  }

  dimension: corridor_id {
    type: number
    sql: ${TABLE}.CORRIDOR_ID ;;
  }

  dimension: corridor_name_txt {
    type: string
    sql: ${TABLE}.CORRIDOR_NAME_TXT ;;
  }

  dimension: corridor_sk {
    type: number
    sql: ${TABLE}.CORRIDOR_SK ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: last_operated_by_txt {
    type: string
    sql: ${TABLE}.LAST_OPERATED_BY_TXT ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: mm_end_flt {
    type: number
    sql: ${TABLE}.MM_END_FLT ;;
  }

  dimension: mm_start_flt {
    type: number
    sql: ${TABLE}.MM_START_FLT ;;
  }

  dimension: primary_direction_desc {
    type: string
    sql: ${TABLE}.PRIMARY_DIRECTION_DESC ;;
  }

  dimension: primary_poi_txt {
    type: string
    sql: ${TABLE}.PRIMARY_POI_TXT ;;
  }

  dimension: primary_type_direction_cd {
    type: number
    sql: ${TABLE}.PRIMARY_TYPE_DIRECTION_CD ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: secondary_direction_desc {
    type: string
    sql: ${TABLE}.SECONDARY_DIRECTION_DESC ;;
  }

  dimension: secondary_poi_txt {
    type: string
    sql: ${TABLE}.SECONDARY_POI_TXT ;;
  }

  dimension: secondary_type_direction_cd {
    type: number
    sql: ${TABLE}.SECONDARY_TYPE_DIRECTION_CD ;;
  }

  dimension: security_group_id {
    type: number
    sql: ${TABLE}.SECURITY_GROUP_ID ;;
  }

  dimension: type_corridor_cd {
    type: number
    sql: ${TABLE}.TYPE_CORRIDOR_CD ;;
  }

  dimension: type_corridor_desc {
    type: string
    sql: ${TABLE}.TYPE_CORRIDOR_DESC ;;
  }

  dimension: type_corridor_status_cd {
    type: number
    sql: ${TABLE}.TYPE_CORRIDOR_STATUS_CD ;;
  }

  dimension: type_corridor_status_desc {
    type: string
    sql: ${TABLE}.TYPE_CORRIDOR_STATUS_DESC ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: vsl_primary_dir_enable_flg {
    type: number
    sql: ${TABLE}.VSL_PRIMARY_DIR_ENABLE_FLG ;;
  }

  dimension: vsl_secondary_dir_enable_flg {
    type: number
    sql: ${TABLE}.VSL_SECONDARY_DIR_ENABLE_FLG ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
