view: dim_type_wr_road_condition {
  sql_table_name: CTMR_MART.DIM_TYPE_WR_ROAD_CONDITION ;;

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: type_road_cond_cat_long {
    type: string
    sql: ${TABLE}.TYPE_ROAD_COND_CAT_LONG ;;
  }

  dimension: type_road_cond_cat_short {
    type: string
    sql: ${TABLE}.TYPE_ROAD_COND_CAT_SHORT ;;
  }

  dimension: type_road_cond_cat_sort {
    type: number
    sql: ${TABLE}.TYPE_ROAD_COND_CAT_SORT ;;
  }

  dimension: type_road_cond_cat_value {
    type: string
    sql: ${TABLE}.TYPE_ROAD_COND_CAT_VALUE ;;
  }

  dimension: type_road_cond_category_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_COND_CATEGORY_CD ;;
  }

  dimension: type_wr_road_cond_long {
    type: string
    sql: ${TABLE}.TYPE_WR_ROAD_COND_LONG ;;
  }

  dimension: type_wr_road_cond_short {
    type: string
    sql: ${TABLE}.TYPE_WR_ROAD_COND_SHORT ;;
  }

  dimension: type_wr_road_cond_sort {
    type: number
    sql: ${TABLE}.TYPE_WR_ROAD_COND_SORT ;;
  }

  dimension: type_wr_road_cond_value {
    type: string
    sql: ${TABLE}.TYPE_WR_ROAD_COND_VALUE ;;
  }

  dimension: type_wr_road_condition_cd {
    type: number
    sql: ${TABLE}.TYPE_WR_ROAD_CONDITION_CD ;;
  }

  dimension: type_wr_road_condition_sk {
    type: number
    sql: ${TABLE}.TYPE_WR_ROAD_CONDITION_SK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
