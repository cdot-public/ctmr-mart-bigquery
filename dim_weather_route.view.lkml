view: dim_weather_route {
  sql_table_name: CTMR_MART.DIM_WEATHER_ROUTE ;;

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: last_data_owner_txt {
    type: string
    sql: ${TABLE}.LAST_DATA_OWNER_TXT ;;
  }

  dimension_group: last_data_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_DATA_UPDATE_DT ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: mm_end_flt {
    type: number
    sql: ${TABLE}.MM_END_FLT ;;
  }

  dimension: mm_start_flt {
    type: number
    sql: ${TABLE}.MM_START_FLT ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: security_group_id {
    type: number
    sql: ${TABLE}.SECURITY_GROUP_ID ;;
  }

  dimension: security_group_name {
    type: string
    sql: ${TABLE}.SECURITY_GROUP_NAME ;;
  }

  dimension: type_road_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_CD ;;
  }

  dimension: type_road_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC ;;
  }

  dimension: type_road_short_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_SHORT_DESC ;;
  }

  dimension: type_weather_route_group_cd {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_ROUTE_GROUP_CD ;;
  }

  dimension: type_weather_route_status_cd {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_ROUTE_STATUS_CD ;;
  }

  dimension: type_wr_group_long {
    type: string
    sql: ${TABLE}.TYPE_WR_GROUP_LONG ;;
  }

  dimension: type_wr_group_short {
    type: string
    sql: ${TABLE}.TYPE_WR_GROUP_SHORT ;;
  }

  dimension: type_wr_group_sort {
    type: number
    sql: ${TABLE}.TYPE_WR_GROUP_SORT ;;
  }

  dimension: type_wr_group_value {
    type: string
    sql: ${TABLE}.TYPE_WR_GROUP_VALUE ;;
  }

  dimension: type_wr_status_long {
    type: string
    sql: ${TABLE}.TYPE_WR_STATUS_LONG ;;
  }

  dimension: type_wr_status_short {
    type: string
    sql: ${TABLE}.TYPE_WR_STATUS_SHORT ;;
  }

  dimension: type_wr_status_sort {
    type: number
    sql: ${TABLE}.TYPE_WR_STATUS_SORT ;;
  }

  dimension: type_wr_status_value {
    type: string
    sql: ${TABLE}.TYPE_WR_STATUS_VALUE ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: weather_route_common_name_txt {
    type: string
    sql: ${TABLE}.WEATHER_ROUTE_COMMON_NAME_TXT ;;
  }

  dimension: weather_route_desc_txt {
    type: string
    sql: ${TABLE}.WEATHER_ROUTE_DESC_TXT ;;
  }

  dimension: weather_route_id {
    type: number
    sql: ${TABLE}.WEATHER_ROUTE_ID ;;
  }

  dimension: weather_route_length_miles_flt {
    type: number
    sql: ${TABLE}.WEATHER_ROUTE_LENGTH_MILES_FLT ;;
  }

  dimension: weather_route_sk {
    type: number
    sql: ${TABLE}.WEATHER_ROUTE_SK ;;
  }

  measure: count {
    type: count
    drill_fields: [security_group_name]
  }
}
