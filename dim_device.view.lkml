view: dim_device {
  sql_table_name: CTMR_MART.DIM_DEVICE ;;

  dimension: capacity {
    type: number
    sql: ${TABLE}.CAPACITY ;;
  }

  dimension: city_id {
    type: number
    sql: ${TABLE}.CITY_ID ;;
  }

  dimension: common_name_txt {
    type: string
    sql: ${TABLE}.COMMON_NAME_TXT ;;
  }

  dimension: county_id {
    type: number
    sql: ${TABLE}.COUNTY_ID ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: device_id {
    type: number
    sql: ${TABLE}.DEVICE_ID ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dgl_latitude_flt {
    type: number
    sql: ${TABLE}.DGL_LATITUDE_FLT ;;
  }

  dimension: dgl_longitude_flt {
    type: number
    sql: ${TABLE}.DGL_LONGITUDE_FLT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: group_id {
    type: number
    sql: ${TABLE}.GROUP_ID ;;
  }

  dimension: in_use_flg {
    type: number
    sql: ${TABLE}.IN_USE_FLG ;;
  }

  dimension: ipaddress_txt {
    type: string
    sql: ${TABLE}.IPADDRESS_TXT ;;
  }

  dimension_group: last_poll_time_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_POLL_TIME_DT ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: latitude_flt {
    type: number
    sql: ${TABLE}.LATITUDE_FLT ;;
  }

  dimension: longitude_flt {
    type: number
    sql: ${TABLE}.LONGITUDE_FLT ;;
  }

  dimension: maintenance_group_id {
    type: number
    sql: ${TABLE}.MAINTENANCE_GROUP_ID ;;
  }

  dimension: maintenance_group_name {
    type: string
    sql: ${TABLE}.MAINTENANCE_GROUP_NAME ;;
  }

  dimension: manufacturer_id {
    type: number
    sql: ${TABLE}.MANUFACTURER_ID ;;
  }

  dimension: mat_id {
    type: number
    sql: ${TABLE}.MAT_ID ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: notes_txt {
    type: string
    sql: ${TABLE}.NOTES_TXT ;;
  }

  dimension: polling_enabled_flg {
    type: number
    sql: ${TABLE}.POLLING_ENABLED_FLG ;;
  }

  dimension: polling_interval_num {
    type: number
    sql: ${TABLE}.POLLING_INTERVAL_NUM ;;
  }

  dimension: port_num {
    type: number
    sql: ${TABLE}.PORT_NUM ;;
  }

  dimension: post_ttt_enabled_flg {
    type: number
    sql: ${TABLE}.POST_TTT_ENABLED_FLG ;;
  }

  dimension: primary_segment_offset_num {
    type: number
    sql: ${TABLE}.PRIMARY_SEGMENT_OFFSET_NUM ;;
  }

  dimension: primary_vol_threshold_num {
    type: number
    sql: ${TABLE}.PRIMARY_VOL_THRESHOLD_NUM ;;
  }

  dimension: public_common_name_txt {
    type: string
    sql: ${TABLE}.PUBLIC_COMMON_NAME_TXT ;;
  }

  dimension: publish_to_web_flg {
    type: number
    sql: ${TABLE}.PUBLISH_TO_WEB_FLG ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: sap_id {
    type: number
    sql: ${TABLE}.SAP_ID ;;
  }

  dimension: sap_name_txt {
    type: string
    sql: ${TABLE}.SAP_NAME_TXT ;;
  }

  dimension: secondary_segment_offset_num {
    type: number
    sql: ${TABLE}.SECONDARY_SEGMENT_OFFSET_NUM ;;
  }

  dimension: secondary_vol_threshold_num {
    type: number
    sql: ${TABLE}.SECONDARY_VOL_THRESHOLD_NUM ;;
  }

  dimension: security_group_name {
    type: string
    sql: ${TABLE}.SECURITY_GROUP_NAME ;;
  }

  dimension: sync_dsply_actl_loc_flg {
    type: number
    sql: ${TABLE}.SYNC_DSPLY_ACTL_LOC_FLG ;;
  }

  dimension: type_access_cd {
    type: number
    sql: ${TABLE}.TYPE_ACCESS_CD ;;
  }

  dimension: type_access_code_value {
    type: string
    sql: ${TABLE}.TYPE_ACCESS_CODE_VALUE ;;
  }

  dimension: type_access_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ACCESS_LONG_DESC ;;
  }

  dimension: type_access_short_desc {
    type: string
    sql: ${TABLE}.TYPE_ACCESS_SHORT_DESC ;;
  }

  dimension: type_access_sort_order {
    type: number
    sql: ${TABLE}.TYPE_ACCESS_SORT_ORDER ;;
  }

  dimension: type_device_cd {
    type: number
    sql: ${TABLE}.TYPE_DEVICE_CD ;;
  }

  dimension: type_device_code_value {
    type: string
    sql: ${TABLE}.TYPE_DEVICE_CODE_VALUE ;;
  }

  dimension: type_device_long_desc {
    type: string
    sql: ${TABLE}.TYPE_DEVICE_LONG_DESC ;;
  }

  dimension: type_device_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DEVICE_SHORT_DESC ;;
  }

  dimension: type_device_sort_order {
    type: number
    sql: ${TABLE}.TYPE_DEVICE_SORT_ORDER ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_code_value {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_CODE_VALUE ;;
  }

  dimension: type_direction_long_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_LONG_DESC ;;
  }

  dimension: type_direction_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_SHORT_DESC ;;
  }

  dimension: type_direction_sort_order {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_SORT_ORDER ;;
  }

  dimension: type_group_long {
    type: string
    sql: ${TABLE}.TYPE_GROUP_LONG ;;
  }

  dimension: type_group_short {
    type: string
    sql: ${TABLE}.TYPE_GROUP_SHORT ;;
  }

  dimension: type_road_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_CD ;;
  }

  dimension: type_road_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC ;;
  }

  dimension: type_road_short_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_SHORT_DESC ;;
  }

  dimension: type_timezone_cd {
    type: number
    sql: ${TABLE}.TYPE_TIMEZONE_CD ;;
  }

  dimension: type_timezone_code_value {
    type: string
    sql: ${TABLE}.TYPE_TIMEZONE_CODE_VALUE ;;
  }

  dimension: type_timezone_long_desc {
    type: string
    sql: ${TABLE}.TYPE_TIMEZONE_LONG_DESC ;;
  }

  dimension: type_timezone_short_desc {
    type: string
    sql: ${TABLE}.TYPE_TIMEZONE_SHORT_DESC ;;
  }

  dimension: type_timezone_sort_order {
    type: number
    sql: ${TABLE}.TYPE_TIMEZONE_SORT_ORDER ;;
  }

  measure: count {
    type: count
    drill_fields: [maintenance_group_name, security_group_name]
  }
}
