view: dim_its_canned_response {
  sql_table_name: CTMR_MART.DIM_ITS_CANNED_RESPONSE ;;

  dimension: canned_id {
    type: number
    sql: ${TABLE}.CANNED_ID ;;
  }

  dimension: canned_sk {
    type: number
    sql: ${TABLE}.CANNED_SK ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATED ;;
  }

  dimension: dept_id {
    type: number
    sql: ${TABLE}.DEPT_ID ;;
  }

  dimension: dept_name {
    type: string
    sql: ${TABLE}.DEPT_NAME ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: isenabled {
    type: number
    sql: ${TABLE}.ISENABLED ;;
  }

  dimension: lang {
    type: string
    sql: ${TABLE}.LANG ;;
  }

  dimension: notes {
    type: string
    sql: ${TABLE}.NOTES ;;
  }

  dimension: response {
    type: string
    sql: ${TABLE}.RESPONSE ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.TITLE ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.UPDATED ;;
  }

  measure: count {
    type: count
    drill_fields: [dept_name]
  }
}
