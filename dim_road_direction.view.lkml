view: dim_road_direction {
  sql_table_name: CTMR_MART.DIM_ROAD_DIRECTION ;;

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: road_abbr_txt {
    type: string
    sql: ${TABLE}.ROAD_ABBR_TXT ;;
  }

  dimension: road_direction_id {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_ID ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: sort_order_num {
    type: number
    sql: ${TABLE}.SORT_ORDER_NUM ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_long_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_LONG_DESC ;;
  }

  dimension: type_direction_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_SHORT_DESC ;;
  }

  dimension: type_direction_sort {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_SORT ;;
  }

  dimension: type_direction_value {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_VALUE ;;
  }

  dimension: type_road_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_CD ;;
  }

  dimension: type_road_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC ;;
  }

  dimension: type_road_short_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_SHORT_DESC ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
