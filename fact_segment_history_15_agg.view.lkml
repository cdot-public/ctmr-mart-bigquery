view: fact_segment_history_15_agg {
  sql_table_name: CTMR_MART.FACT_SEGMENT_HISTORY_15_AGG ;;

  dimension: avg_calculated_speed_flt {
    type: number
    sql: ${TABLE}.AVG_CALCULATED_SPEED_FLT ;;
  }

  dimension: avg_calculated_speed_w_volume {
    type: number
    sql: ${TABLE}.AVG_CALCULATED_SPEED_W_VOLUME ;;
  }

  dimension: avg_occupancy_flt {
    type: number
    sql: ${TABLE}.AVG_OCCUPANCY_FLT ;;
  }

  dimension: avg_speed_flt {
    type: number
    sql: ${TABLE}.AVG_SPEED_FLT ;;
  }

  dimension: avg_speed_w_volume {
    type: number
    sql: ${TABLE}.AVG_SPEED_W_VOLUME ;;
  }

  dimension: avg_system_calculated_tt {
    type: number
    sql: ${TABLE}.AVG_SYSTEM_CALCULATED_TT ;;
  }

  dimension: avg_travel_time {
    type: number
    sql: ${TABLE}.AVG_TRAVEL_TIME ;;
  }

  dimension: avg_volume_flt {
    type: number
    sql: ${TABLE}.AVG_VOLUME_FLT ;;
  }

  dimension: calculated_dt_sk {
    type: number
    sql: ${TABLE}.CALCULATED_DT_SK ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension_group: first_reading_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FIRST_READING_DT ;;
  }

  dimension: grp_15_min {
    type: string
    sql: ${TABLE}.GRP_15_MIN ;;
  }

  dimension_group: last_reading_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_READING_DT ;;
  }

  dimension: segment_history_15_agg_sk {
    type: number
    sql: ${TABLE}.SEGMENT_HISTORY_15_AGG_SK ;;
  }

  dimension: segment_sk {
    type: number
    sql: ${TABLE}.SEGMENT_SK ;;
  }

  dimension: total_reading_ct {
    type: number
    sql: ${TABLE}.TOTAL_READING_CT ;;
  }

  dimension: total_volume_flt {
    type: number
    sql: ${TABLE}.TOTAL_VOLUME_FLT ;;
  }

  dimension: valid_occupancy_reading_ct {
    type: number
    sql: ${TABLE}.VALID_OCCUPANCY_READING_CT ;;
  }

  dimension: valid_speed_reading_ct {
    type: number
    sql: ${TABLE}.VALID_SPEED_READING_CT ;;
  }

  dimension: valid_volume_reading_ct {
    type: number
    sql: ${TABLE}.VALID_VOLUME_READING_CT ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
