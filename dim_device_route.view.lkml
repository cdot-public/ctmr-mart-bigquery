view: dim_device_route {
  sql_table_name: CTMR_MART.DIM_DEVICE_ROUTE ;;

  dimension: device_id {
    type: number
    sql: ${TABLE}.DEVICE_ID ;;
  }

  dimension: device_mm {
    type: number
    sql: ${TABLE}.DEVICE_MM ;;
  }

  dimension: device_route_sk {
    type: number
    sql: ${TABLE}.DEVICE_ROUTE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: route_id {
    type: number
    sql: ${TABLE}.ROUTE_ID ;;
  }

  dimension: route_name_txt {
    type: string
    sql: ${TABLE}.ROUTE_NAME_TXT ;;
  }

  dimension: segment_id {
    type: number
    sql: ${TABLE}.SEGMENT_ID ;;
  }

  dimension: segment_mm_end {
    type: number
    sql: ${TABLE}.SEGMENT_MM_END ;;
  }

  dimension: segment_mm_start {
    type: number
    sql: ${TABLE}.SEGMENT_MM_START ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
