view: fact_segment_history_60_denorm {
  sql_table_name: CTMR_MART.fact_segment_history_60_denorm ;;

  dimension: avg_calculated_speed_flt {
    type: number
    sql: ${TABLE}.AVG_CALCULATED_SPEED_FLT ;;
  }

  dimension: avg_calculated_speed_w_volume {
    type: number
    sql: ${TABLE}.AVG_CALCULATED_SPEED_W_VOLUME ;;
  }

  dimension: avg_occupancy_flt {
    type: number
    sql: ${TABLE}.AVG_OCCUPANCY_FLT ;;
  }

  dimension: avg_speed_flt {
    type: number
    sql: ${TABLE}.AVG_SPEED_FLT ;;
  }

  dimension: avg_speed_w_volume {
    type: number
    sql: ${TABLE}.AVG_SPEED_W_VOLUME ;;
  }

  dimension: avg_system_calculated_tt {
    type: number
    sql: ${TABLE}.AVG_SYSTEM_CALCULATED_TT ;;
  }

  dimension: avg_travel_time {
    type: number
    sql: ${TABLE}.AVG_TRAVEL_TIME ;;
  }

  dimension: avg_volume_flt {
    type: number
    sql: ${TABLE}.AVG_VOLUME_FLT ;;
  }

  dimension: calculated_dt_sk {
    type: number
    sql: ${TABLE}.CALCULATED_DT_SK ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension_group: first_reading_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FIRST_READING_DT ;;
  }

  dimension: grp_60_min {
    type: string
    sql: ${TABLE}.GRP_60_MIN ;;
  }

  dimension_group: last_reading_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_READING_DT ;;
  }

  dimension: mm_end_flt {
    type: number
    sql: ${TABLE}.MM_END_FLT ;;
  }

  dimension: mm_start_flt {
    type: number
    sql: ${TABLE}.MM_START_FLT ;;
  }

  dimension: pti {
    type: number
    sql: ${TABLE}.PTI ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: sctt_percentile_95 {
    type: number
    sql: ${TABLE}.SCTT_PERCENTILE_95 ;;
  }

  dimension: segment_common_name_txt {
    type: string
    sql: ${TABLE}.SEGMENT_COMMON_NAME_TXT ;;
  }

  dimension: segment_history_60_agg_sk {
    type: number
    sql: ${TABLE}.SEGMENT_HISTORY_60_AGG_SK ;;
  }

  dimension: segment_length_miles_flt {
    type: number
    sql: ${TABLE}.SEGMENT_LENGTH_MILES_FLT ;;
  }

  dimension: segment_sk {
    type: number
    sql: ${TABLE}.SEGMENT_SK ;;
  }

  dimension: speed_limit_num {
    type: number
    sql: ${TABLE}.SPEED_LIMIT_NUM ;;
  }

  dimension: total_reading_ct {
    type: number
    sql: ${TABLE}.TOTAL_READING_CT ;;
  }

  dimension: total_volume_flt {
    type: number
    sql: ${TABLE}.TOTAL_VOLUME_FLT ;;
  }

  dimension: type_direction_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_SHORT_DESC ;;
  }

  dimension: type_road_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC ;;
  }

  dimension: type_segment_short_desc {
    type: string
    sql: ${TABLE}.TYPE_SEGMENT_SHORT_DESC ;;
  }

  dimension: valid_occupancy_reading_ct {
    type: number
    sql: ${TABLE}.VALID_OCCUPANCY_READING_CT ;;
  }

  dimension: valid_speed_reading_ct {
    type: number
    sql: ${TABLE}.VALID_SPEED_READING_CT ;;
  }

  dimension: valid_volume_reading_ct {
    type: number
    sql: ${TABLE}.VALID_VOLUME_READING_CT ;;
  }
  dimension: difference_from_speed_limit {
    type: number
    sql: ${avg_calculated_speed_flt} - ${speed_limit_num} ;;
  }

  measure: avg_speed_flt_measure {
    type: average
    sql: ${TABLE}.AVG_SPEED_FLT ;;
  }

  measure: sum_total_volume {
    type: sum
    sql: ${TABLE}.TOTAL_VOLUME_FLT ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
