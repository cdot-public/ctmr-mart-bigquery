view: dim_its_repairkit {
  sql_table_name: CTMR_MART.DIM_ITS_REPAIRKIT ;;

  dimension_group: adddate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ADDDATE ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: editor {
    type: string
    sql: ${TABLE}.EDITOR ;;
  }

  dimension: isenabled {
    type: number
    sql: ${TABLE}.ISENABLED ;;
  }

  dimension: kit {
    type: string
    sql: ${TABLE}.KIT ;;
  }

  dimension: kittype {
    type: string
    sql: ${TABLE}.KITTYPE ;;
  }

  dimension: repairkit_id {
    type: number
    sql: ${TABLE}.REPAIRKIT_ID ;;
  }

  dimension: repairkit_pid {
    type: number
    value_format_name: id
    sql: ${TABLE}.REPAIRKIT_PID ;;
  }

  dimension: repairkit_sk {
    type: number
    sql: ${TABLE}.REPAIRKIT_SK ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.UPDATED ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
