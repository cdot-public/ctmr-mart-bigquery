view: fact_event_construction {
  sql_table_name: CTMR_MART.FACT_EVENT_CONSTRUCTION ;;

  dimension: cell_phone_txt {
    type: string
    sql: ${TABLE}.CELL_PHONE_TXT ;;
  }

  dimension: contractor_info_txt {
    type: string
    sql: ${TABLE}.CONTRACTOR_INFO_TXT ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: event_construction_id {
    type: number
    sql: ${TABLE}.EVENT_CONSTRUCTION_ID ;;
  }

  dimension: event_construction_sk {
    type: number
    sql: ${TABLE}.EVENT_CONSTRUCTION_SK ;;
  }

  dimension: event_sk {
    type: number
    sql: ${TABLE}.EVENT_SK ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension: first_name_txt {
    type: string
    sql: ${TABLE}.FIRST_NAME_TXT ;;
  }

  dimension: last_name_txt {
    type: string
    sql: ${TABLE}.LAST_NAME_TXT ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: other_phone_txt {
    type: string
    sql: ${TABLE}.OTHER_PHONE_TXT ;;
  }

  dimension_group: project_end_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.PROJECT_END_DT ;;
  }

  dimension: project_ref_txt {
    type: string
    sql: ${TABLE}.PROJECT_REF_TXT ;;
  }

  dimension_group: project_start_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.PROJECT_START_DT ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
