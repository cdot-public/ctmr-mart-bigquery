view: dim_corridor_device {
  sql_table_name: CTMR_MART.DIM_CORRIDOR_DEVICE ;;

  dimension: corridor_device_sk {
    type: number
    sql: ${TABLE}.CORRIDOR_DEVICE_SK ;;
  }

  dimension: corridor_id {
    type: number
    sql: ${TABLE}.CORRIDOR_ID ;;
  }

  dimension: corridor_mm_end {
    type: number
    sql: ${TABLE}.CORRIDOR_MM_END ;;
  }

  dimension: corridor_mm_start {
    type: number
    sql: ${TABLE}.CORRIDOR_MM_START ;;
  }

  dimension: corridor_name_txt {
    type: string
    sql: ${TABLE}.CORRIDOR_NAME_TXT ;;
  }

  dimension: corridor_primary_direction {
    type: string
    sql: ${TABLE}.CORRIDOR_PRIMARY_DIRECTION ;;
  }

  dimension: corridor_secondary_direction {
    type: string
    sql: ${TABLE}.CORRIDOR_SECONDARY_DIRECTION ;;
  }

  dimension: device_id {
    type: number
    sql: ${TABLE}.DEVICE_ID ;;
  }

  dimension: device_name_txt {
    type: string
    sql: ${TABLE}.DEVICE_NAME_TXT ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: gantry_name_txt {
    type: string
    sql: ${TABLE}.GANTRY_NAME_TXT ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: security_group_id {
    type: number
    sql: ${TABLE}.SECURITY_GROUP_ID ;;
  }

  dimension: security_group_name {
    type: string
    sql: ${TABLE}.SECURITY_GROUP_NAME ;;
  }

  dimension: type_corridor_long {
    type: string
    sql: ${TABLE}.TYPE_CORRIDOR_LONG ;;
  }

  dimension: type_corridor_short {
    type: string
    sql: ${TABLE}.TYPE_CORRIDOR_SHORT ;;
  }

  dimension: type_corridor_status_cd {
    type: number
    sql: ${TABLE}.TYPE_CORRIDOR_STATUS_CD ;;
  }

  dimension: type_device_cd {
    type: number
    sql: ${TABLE}.TYPE_DEVICE_CD ;;
  }

  dimension: type_device_desc {
    type: string
    sql: ${TABLE}.TYPE_DEVICE_DESC ;;
  }

  dimension: type_device_status_cd {
    type: number
    sql: ${TABLE}.TYPE_DEVICE_STATUS_CD ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_DESC ;;
  }

  dimension: vsl_primary_dir_enable_flg {
    type: number
    sql: ${TABLE}.VSL_PRIMARY_DIR_ENABLE_FLG ;;
  }

  dimension: vsl_secondary_dir_enable_flg {
    type: number
    sql: ${TABLE}.VSL_SECONDARY_DIR_ENABLE_FLG ;;
  }

  measure: count {
    type: count
    drill_fields: [security_group_name]
  }
}
