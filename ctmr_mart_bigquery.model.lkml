connection: "ctmr_mart"

# include all the views
include: "*.view"

datagroup: ctmr_mart_bigquery_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: ctmr_mart_bigquery_default_datagroup

explore: dim_agency_lane_agg {}

explore: dim_agency_responder {}

explore: dim_agency_services_agg {}

explore: dim_atr_daily {}

explore: dim_autoscope {}

explore: dim_avi {}

explore: dim_avi_pair {}

explore: dim_avi_pair_segment {}

explore: dim_calendar {}

explore: dim_cctv_camera {}

explore: dim_corridor {}

explore: dim_corridor_device {}

explore: dim_crossroad {}

explore: dim_device {}

explore: dim_device_route {}

explore: dim_device_segment {}

explore: dim_dms {}

explore: dim_doppler {}

explore: dim_gantry {}

explore: dim_gantry_device {}

explore: dim_its_canned_response {}

explore: dim_its_department {}

explore: dim_its_device {}

explore: dim_its_repairkit {}

explore: dim_its_staff {}

explore: dim_its_ticket_priority {}

explore: dim_its_vehicle {}

explore: dim_lane {}

explore: dim_rm {}

explore: dim_road {}

explore: dim_road_direction {}

explore: dim_route {}

explore: dim_route_device {}

explore: dim_route_event {}

explore: dim_route_weather_route {}

explore: dim_rtms {}

explore: dim_rtms_lane {}

explore: dim_rtms_lane2 {}

explore: dim_rtms_lane3 {}

explore: dim_segment {}

explore: dim_segment4 {}

explore: dim_segment_orig {}

explore: dim_speed_device_direction {}

explore: dim_time {}

explore: dim_tv_camera {}

explore: dim_tv_camera_polling {}

explore: dim_type_agency_dispatch {}

explore: dim_type_agency_dispatch_incid {}

explore: dim_type_agency_dispatch_srce {}

explore: dim_type_agency_no_service {}

explore: dim_type_call_log_agg {}

explore: dim_type_chain_law {}

explore: dim_type_cmv {}

explore: dim_type_cmv_state {}

explore: dim_type_direction {}

explore: dim_type_escape_ramp_loc {}

explore: dim_type_lus_message {}

explore: dim_type_priority_message {}

explore: dim_type_roadway_closure {}

explore: dim_type_rwis_surface_status {}

explore: dim_type_vehicle {}

explore: dim_type_wr_road_condition {}

explore: dim_weather_route {}

explore: dim_weather_st_sensor {}

explore: dim_weather_station {}

explore: dim_wr_comment {}

explore: events_denorm {
  join: dim_segment {
    view_label: "dim_segment"
    type: left_outer
    relationship: one_to_one
    sql_on: ${events_denorm.road_id} = ${dim_segment.road_id}
      AND ${events_denorm.type_direction_cd} = ${dim_segment.type_direction_cd}
      AND ((${events_denorm.mm_start_flt} >= ${dim_segment.mm_start_flt} and ${events_denorm.mm_start_flt} <=${dim_segment.mm_end_flt})
      OR (${events_denorm.mm_end_flt} >= ${dim_segment.mm_start_flt} and ${events_denorm.mm_end_flt} <=${dim_segment.mm_end_flt}));;
  }
  join: geography {
    view_label: "Segment Geography"
    type: left_outer
    relationship: one_to_one
    sql_on: ${dim_segment.segment_id} = ${geography.segment_id} ;;
  }
  join: geography__coordinates {
    view_label: "Segment Geography Coordinates"
    sql: LEFT JOIN UNNEST(${geography.coordinates}) as geography__coordinates ;;
    relationship: one_to_many
  }
}

explore: fact_agency_dispatch {}

explore: fact_atr_daily_axle_bin_data {}

explore: fact_audit_event_summary {}

explore: fact_call_log {}

explore: fact_dms_message {}

explore: fact_dms_poll {}

explore: fact_event {}

explore: fact_event_action_log {}

explore: fact_event_cons_work {}

explore: fact_event_construction {}

explore: fact_event_description {}

explore: fact_its_ticket {}

explore: fact_route_history {}

explore: fact_rwis_surface_data {}

explore: fact_segment_history_15_denorm {}

explore: fact_segment_history_15_agg {}

explore: fact_segment_history_30_agg {}

explore: fact_segment_history_60_agg {}

explore: fact_segment_history_60_denorm {
  join: dim_segment {
    view_label: "dim_segment"
    type: left_outer
    relationship: one_to_one
    sql_on: ${fact_segment_history_60_denorm.segment_sk} = ${dim_segment.segment_sk} ;;
  }
  join: geography {
    view_label: "Segment Geography"
    type: left_outer
    relationship: one_to_one
    sql_on: ${dim_segment.segment_id} = ${geography.segment_id} ;;
  }
  join: geography__coordinates {
    view_label: "Segment Geography Coordinates"
    sql: LEFT JOIN UNNEST(${geography.coordinates}) as geography__coordinates ;;
    relationship: one_to_many
  }
}

explore: fact_tv_incidents {}

explore: fact_vsl_proposed_speed {}
