view: fact_rwis_surface_data {
  sql_table_name: CTMR_MART.FACT_RWIS_SURFACE_DATA ;;

  dimension: chemical_factor_num {
    type: number
    sql: ${TABLE}.CHEMICAL_FACTOR_NUM ;;
  }

  dimension: chemical_percentage_num {
    type: number
    sql: ${TABLE}.CHEMICAL_PERCENTAGE_NUM ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension_group: device_collection_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DEVICE_COLLECTION_DT ;;
  }

  dimension: device_collection_dt_sk {
    type: number
    sql: ${TABLE}.DEVICE_COLLECTION_DT_SK ;;
  }

  dimension: device_collection_tm_sk {
    type: number
    sql: ${TABLE}.DEVICE_COLLECTION_TM_SK ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: ess_subsurface_temperature_num {
    type: number
    sql: ${TABLE}.ESS_SUBSURFACE_TEMPERATURE_NUM ;;
  }

  dimension: ess_surface_conductivity_num {
    type: number
    sql: ${TABLE}.ESS_SURFACE_CONDUCTIVITY_NUM ;;
  }

  dimension: ess_surface_freeze_point_num {
    type: number
    sql: ${TABLE}.ESS_SURFACE_FREEZE_POINT_NUM ;;
  }

  dimension: ess_surface_salinity_num {
    type: number
    sql: ${TABLE}.ESS_SURFACE_SALINITY_NUM ;;
  }

  dimension: ess_surface_temperature_flt {
    type: number
    sql: ${TABLE}.ESS_SURFACE_TEMPERATURE_FLT ;;
  }

  dimension: ess_water_depth_flt {
    type: number
    sql: ${TABLE}.ESS_WATER_DEPTH_FLT ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension: friction_num {
    type: number
    sql: ${TABLE}.FRICTION_NUM ;;
  }

  dimension: ice_percentage_num {
    type: number
    sql: ${TABLE}.ICE_PERCENTAGE_NUM ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: rwis_surface_data_id {
    type: number
    sql: ${TABLE}.RWIS_SURFACE_DATA_ID ;;
  }

  dimension: rwis_surface_data_sk {
    type: number
    sql: ${TABLE}.RWIS_SURFACE_DATA_SK ;;
  }

  dimension_group: system_collection_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.SYSTEM_COLLECTION_DT ;;
  }

  dimension: system_collection_dt_sk {
    type: number
    sql: ${TABLE}.SYSTEM_COLLECTION_DT_SK ;;
  }

  dimension: system_collection_tm_sk {
    type: number
    sql: ${TABLE}.SYSTEM_COLLECTION_TM_SK ;;
  }

  dimension: type_ess_pvmt_sensor_error_cd {
    type: number
    sql: ${TABLE}.TYPE_ESS_PVMT_SENSOR_ERROR_CD ;;
  }

  dimension: type_ess_surface_black_ice_cd {
    type: number
    sql: ${TABLE}.TYPE_ESS_SURFACE_BLACK_ICE_CD ;;
  }

  dimension: type_rwis_surface_status_sk {
    type: number
    sql: ${TABLE}.TYPE_RWIS_SURFACE_STATUS_SK ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: weather_station_sensor_sk {
    type: number
    sql: ${TABLE}.WEATHER_STATION_SENSOR_SK ;;
  }

  dimension: weather_station_sk {
    type: number
    sql: ${TABLE}.WEATHER_STATION_SK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
