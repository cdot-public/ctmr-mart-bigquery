view: dim_its_ticket_priority {
  sql_table_name: CTMR_MART.DIM_ITS_TICKET_PRIORITY ;;

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: ispublic {
    type: number
    sql: ${TABLE}.ISPUBLIC ;;
  }

  dimension: priority {
    type: string
    sql: ${TABLE}.PRIORITY ;;
  }

  dimension: priority_color {
    type: string
    sql: ${TABLE}.PRIORITY_COLOR ;;
  }

  dimension: priority_desc {
    type: string
    sql: ${TABLE}.PRIORITY_DESC ;;
  }

  dimension: priority_id {
    type: number
    sql: ${TABLE}.PRIORITY_ID ;;
  }

  dimension: priority_sk {
    type: number
    sql: ${TABLE}.PRIORITY_SK ;;
  }

  dimension: priority_urgency {
    type: number
    sql: ${TABLE}.PRIORITY_URGENCY ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
