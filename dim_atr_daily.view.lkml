view: dim_atr_daily {
  sql_table_name: CTMR_MART.DIM_ATR_DAILY ;;

  dimension: atr_daily_id {
    type: number
    sql: ${TABLE}.ATR_DAILY_ID ;;
  }

  dimension: atr_daily_sk {
    type: number
    sql: ${TABLE}.ATR_DAILY_SK ;;
  }

  dimension: clock_sync_error_flg {
    type: number
    sql: ${TABLE}.CLOCK_SYNC_ERROR_FLG ;;
  }

  dimension: comm_error_flg {
    type: number
    sql: ${TABLE}.COMM_ERROR_FLG ;;
  }

  dimension: common_name_txt {
    type: string
    sql: ${TABLE}.COMMON_NAME_TXT ;;
  }

  dimension_group: counter_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.COUNTER_DT ;;
  }

  dimension: counter_serial_txt {
    type: string
    sql: ${TABLE}.COUNTER_SERIAL_TXT ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: delete_oldest_file_flg {
    type: number
    sql: ${TABLE}.DELETE_OLDEST_FILE_FLG ;;
  }

  dimension: device_id {
    type: number
    sql: ${TABLE}.DEVICE_ID ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: file_download_error_flg {
    type: number
    sql: ${TABLE}.FILE_DOWNLOAD_ERROR_FLG ;;
  }

  dimension: file_parse_error_flg {
    type: number
    sql: ${TABLE}.FILE_PARSE_ERROR_FLG ;;
  }

  dimension: firmware_version_flt {
    type: number
    sql: ${TABLE}.FIRMWARE_VERSION_FLT ;;
  }

  dimension_group: last_collection_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_COLLECTION_DT ;;
  }

  dimension_group: last_shutdown_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_SHUTDOWN_DT ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: memory_used_num {
    type: number
    sql: ${TABLE}.MEMORY_USED_NUM ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: mmc_installed_flg {
    type: number
    sql: ${TABLE}.MMC_INSTALLED_FLG ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: site_id_txt {
    type: string
    sql: ${TABLE}.SITE_ID_TXT ;;
  }

  dimension: total_memory_num {
    type: number
    sql: ${TABLE}.TOTAL_MEMORY_NUM ;;
  }

  dimension: type_atr_daily_status_cd {
    type: number
    sql: ${TABLE}.TYPE_ATR_DAILY_STATUS_CD ;;
  }

  dimension: type_atr_daily_status_desc {
    type: string
    sql: ${TABLE}.TYPE_ATR_DAILY_STATUS_DESC ;;
  }

  dimension: type_atr_daily_storage_cd {
    type: number
    sql: ${TABLE}.TYPE_ATR_DAILY_STORAGE_CD ;;
  }

  dimension: type_atr_daily_storage_desc {
    type: string
    sql: ${TABLE}.TYPE_ATR_DAILY_STORAGE_DESC ;;
  }

  dimension: type_atr_model_cd {
    type: number
    sql: ${TABLE}.TYPE_ATR_MODEL_CD ;;
  }

  dimension: type_atr_model_desc {
    type: string
    sql: ${TABLE}.TYPE_ATR_MODEL_DESC ;;
  }

  dimension: type_atr_tech_cd {
    type: number
    sql: ${TABLE}.TYPE_ATR_TECH_CD ;;
  }

  dimension: type_atr_tech_desc {
    type: string
    sql: ${TABLE}.TYPE_ATR_TECH_DESC ;;
  }

  dimension: type_device_cd {
    type: number
    sql: ${TABLE}.TYPE_DEVICE_CD ;;
  }

  dimension: type_device_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DEVICE_SHORT_DESC ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_SHORT_DESC ;;
  }

  dimension: type_road_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_CD ;;
  }

  dimension: type_road_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC ;;
  }

  dimension: type_road_short_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_SHORT_DESC ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
