view: dim_lane {
  sql_table_name: CTMR_MART.DIM_LANE ;;

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: device_direction_cd {
    type: number
    sql: ${TABLE}.DEVICE_DIRECTION_CD ;;
  }

  dimension: device_direction_desc {
    type: string
    sql: ${TABLE}.DEVICE_DIRECTION_DESC ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: ignore_flg {
    type: number
    sql: ${TABLE}.IGNORE_FLG ;;
  }

  dimension: ishov_flg {
    type: number
    sql: ${TABLE}.ISHOV_FLG ;;
  }

  dimension: lane_desc_txt {
    type: string
    sql: ${TABLE}.LANE_DESC_TXT ;;
  }

  dimension: lane_id {
    type: number
    sql: ${TABLE}.LANE_ID ;;
  }

  dimension: lane_ident_txt {
    type: string
    sql: ${TABLE}.LANE_IDENT_TXT ;;
  }

  dimension: lane_num {
    type: number
    sql: ${TABLE}.LANE_NUM ;;
  }

  dimension: lane_sk {
    type: number
    sql: ${TABLE}.LANE_SK ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: segment_common_name_txt {
    type: string
    sql: ${TABLE}.SEGMENT_COMMON_NAME_TXT ;;
  }

  dimension: segment_direction_cd {
    type: number
    sql: ${TABLE}.SEGMENT_DIRECTION_CD ;;
  }

  dimension: segment_direction_desc {
    type: string
    sql: ${TABLE}.SEGMENT_DIRECTION_DESC ;;
  }

  dimension: segment_sk {
    type: number
    sql: ${TABLE}.SEGMENT_SK ;;
  }

  dimension: sort_order_num {
    type: number
    sql: ${TABLE}.SORT_ORDER_NUM ;;
  }

  dimension: type_lane_cd {
    type: number
    sql: ${TABLE}.TYPE_LANE_CD ;;
  }

  dimension: type_lane_long_desc {
    type: string
    sql: ${TABLE}.TYPE_LANE_LONG_DESC ;;
  }

  dimension: type_lane_short_desc {
    type: string
    sql: ${TABLE}.TYPE_LANE_SHORT_DESC ;;
  }

  dimension: type_lane_sort {
    type: number
    sql: ${TABLE}.TYPE_LANE_SORT ;;
  }

  dimension: type_lane_value {
    type: string
    sql: ${TABLE}.TYPE_LANE_VALUE ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
