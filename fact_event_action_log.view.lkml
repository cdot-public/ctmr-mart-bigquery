view: fact_event_action_log {
  sql_table_name: CTMR_MART.FACT_EVENT_ACTION_LOG ;;

  dimension: action_desc_txt {
    type: string
    sql: ${TABLE}.ACTION_DESC_TXT ;;
  }

  dimension_group: action_time_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ACTION_TIME_DT ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: event_action_log_id {
    type: number
    sql: ${TABLE}.EVENT_ACTION_LOG_ID ;;
  }

  dimension: event_action_sk {
    type: number
    sql: ${TABLE}.EVENT_ACTION_SK ;;
  }

  dimension: event_sk {
    type: number
    sql: ${TABLE}.EVENT_SK ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: type_event_action_cd {
    type: number
    sql: ${TABLE}.TYPE_EVENT_ACTION_CD ;;
  }

  dimension: type_event_action_long {
    type: string
    sql: ${TABLE}.TYPE_EVENT_ACTION_LONG ;;
  }

  dimension: type_event_action_short {
    type: string
    sql: ${TABLE}.TYPE_EVENT_ACTION_SHORT ;;
  }

  dimension: type_event_action_sort {
    type: number
    sql: ${TABLE}.TYPE_EVENT_ACTION_SORT ;;
  }

  dimension: type_event_action_value {
    type: string
    sql: ${TABLE}.TYPE_EVENT_ACTION_VALUE ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: user_name_txt {
    type: string
    sql: ${TABLE}.USER_NAME_TXT ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
