view: dim_gantry {
  sql_table_name: CTMR_MART.DIM_GANTRY ;;

  dimension: corridor_id {
    type: number
    sql: ${TABLE}.CORRIDOR_ID ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: gantry_id {
    type: number
    sql: ${TABLE}.GANTRY_ID ;;
  }

  dimension: gantry_name_txt {
    type: string
    sql: ${TABLE}.GANTRY_NAME_TXT ;;
  }

  dimension: gantry_sk {
    type: number
    sql: ${TABLE}.GANTRY_SK ;;
  }

  dimension: max_speed_limit_num {
    type: number
    sql: ${TABLE}.MAX_SPEED_LIMIT_NUM ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: min_speed_limit_num {
    type: number
    sql: ${TABLE}.MIN_SPEED_LIMIT_NUM ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_DESC ;;
  }

  dimension: vsl_max_speed_limit_num {
    type: number
    sql: ${TABLE}.VSL_MAX_SPEED_LIMIT_NUM ;;
  }

  dimension: vsl_min_speed_limit_num {
    type: number
    sql: ${TABLE}.VSL_MIN_SPEED_LIMIT_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
