view: dim_its_device {
  sql_table_name: CTMR_MART.DIM_ITS_DEVICE ;;

  dimension_group: adddate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ADDDATE ;;
  }

  dimension: altitude {
    type: number
    sql: ${TABLE}.ALTITUDE ;;
  }

  dimension: cctv_camera_id {
    type: number
    sql: ${TABLE}.CCTV_CAMERA_ID ;;
  }

  dimension: cctv_camera_sk {
    type: number
    sql: ${TABLE}.CCTV_CAMERA_SK ;;
  }

  dimension: commroute_id {
    type: number
    sql: ${TABLE}.COMMROUTE_ID ;;
  }

  dimension: comms_id {
    type: number
    sql: ${TABLE}.COMMS_ID ;;
  }

  dimension: contact_warranty {
    type: string
    sql: ${TABLE}.CONTACT_WARRANTY ;;
  }

  dimension: crossroad {
    type: string
    sql: ${TABLE}.CROSSROAD ;;
  }

  dimension: ctms_device_id {
    type: number
    sql: ${TABLE}.CTMS_DEVICE_ID ;;
  }

  dimension: ctms_device_sk {
    type: number
    sql: ${TABLE}.CTMS_DEVICE_SK ;;
  }

  dimension: dev_ip {
    type: string
    sql: ${TABLE}.DEV_IP ;;
  }

  dimension: device {
    type: string
    sql: ${TABLE}.DEVICE ;;
  }

  dimension: device_cid {
    type: number
    value_format_name: id
    sql: ${TABLE}.DEVICE_CID ;;
  }

  dimension: device_pid {
    type: number
    value_format_name: id
    sql: ${TABLE}.DEVICE_PID ;;
  }

  dimension: device_purpose {
    type: string
    sql: ${TABLE}.DEVICE_PURPOSE ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: direction {
    type: string
    sql: ${TABLE}.DIRECTION ;;
  }

  dimension: editor {
    type: string
    sql: ${TABLE}.EDITOR ;;
  }

  dimension_group: end_warranty {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.END_WARRANTY ;;
  }

  dimension: equip_access {
    type: string
    sql: ${TABLE}.EQUIP_ACCESS ;;
  }

  dimension_group: install {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.INSTALL_DATE ;;
  }

  dimension: installer_id {
    type: number
    sql: ${TABLE}.INSTALLER_ID ;;
  }

  dimension: isenabled {
    type: number
    sql: ${TABLE}.ISENABLED ;;
  }

  dimension: its_device_id {
    type: number
    sql: ${TABLE}.ITS_DEVICE_ID ;;
  }

  dimension: its_device_sk {
    type: number
    sql: ${TABLE}.ITS_DEVICE_SK ;;
  }

  dimension: latitude {
    type: number
    sql: ${TABLE}.LATITUDE ;;
  }

  dimension: longitude {
    type: number
    sql: ${TABLE}.LONGITUDE ;;
  }

  dimension: maint_id {
    type: number
    sql: ${TABLE}.MAINT_ID ;;
  }

  dimension: manufacture {
    type: string
    sql: ${TABLE}.MANUFACTURE ;;
  }

  dimension: mileage {
    type: number
    sql: ${TABLE}.MILEAGE ;;
  }

  dimension: model {
    type: string
    sql: ${TABLE}.MODEL ;;
  }

  dimension: node_id {
    type: string
    sql: ${TABLE}.NODE_ID ;;
  }

  dimension: num_images {
    type: number
    sql: ${TABLE}.NUM_IMAGES ;;
  }

  dimension_group: pmdated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.PMDATED ;;
  }

  dimension: power_id {
    type: number
    sql: ${TABLE}.POWER_ID ;;
  }

  dimension: road_closure {
    type: string
    sql: ${TABLE}.ROAD_CLOSURE ;;
  }

  dimension: roadway {
    type: string
    sql: ${TABLE}.ROADWAY ;;
  }

  dimension: sapno {
    type: number
    sql: ${TABLE}.SAPNO ;;
  }

  dimension_group: start_warranty {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.START_WARRANTY ;;
  }

  dimension: sw_input_alt {
    type: number
    sql: ${TABLE}.SW_INPUT_ALT ;;
  }

  dimension: travel_time {
    type: string
    sql: ${TABLE}.TRAVEL_TIME ;;
  }

  dimension: type_id {
    type: number
    sql: ${TABLE}.TYPE_ID ;;
  }

  dimension: type_name {
    type: string
    sql: ${TABLE}.TYPE_NAME ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.UPDATED ;;
  }

  measure: count {
    type: count
    drill_fields: [type_name]
  }
}
