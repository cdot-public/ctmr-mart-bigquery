view: dim_type_lus_message {
  sql_table_name: CTMR_MART.DIM_TYPE_LUS_MESSAGE ;;

  dimension: code_value_txt {
    type: string
    sql: ${TABLE}.CODE_VALUE_TXT ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: long_desc_txt {
    type: string
    sql: ${TABLE}.LONG_DESC_TXT ;;
  }

  dimension: short_desc_txt {
    type: string
    sql: ${TABLE}.SHORT_DESC_TXT ;;
  }

  dimension: sort_order_num {
    type: number
    sql: ${TABLE}.SORT_ORDER_NUM ;;
  }

  dimension: type_lus_message_cd {
    type: number
    sql: ${TABLE}.TYPE_LUS_MESSAGE_CD ;;
  }

  dimension: type_lus_message_sk {
    type: number
    sql: ${TABLE}.TYPE_LUS_MESSAGE_SK ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
