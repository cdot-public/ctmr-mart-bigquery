view: dim_route_event {
  sql_table_name: CTMR_MART.DIM_ROUTE_EVENT ;;

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_begin {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_BEGIN_DATE ;;
  }

  dimension_group: dim_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_END_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension: event_end_mm {
    type: number
    sql: ${TABLE}.EVENT_END_MM ;;
  }

  dimension: event_id {
    type: number
    sql: ${TABLE}.EVENT_ID ;;
  }

  dimension: event_sk {
    type: number
    sql: ${TABLE}.EVENT_SK ;;
  }

  dimension: event_start_mm {
    type: number
    sql: ${TABLE}.EVENT_START_MM ;;
  }

  dimension: route_end_mm {
    type: number
    sql: ${TABLE}.ROUTE_END_MM ;;
  }

  dimension: route_end_road_id {
    type: number
    sql: ${TABLE}.ROUTE_END_ROAD_ID ;;
  }

  dimension: route_end_road_sk {
    type: number
    sql: ${TABLE}.ROUTE_END_ROAD_SK ;;
  }

  dimension: route_event_sk {
    type: number
    sql: ${TABLE}.ROUTE_EVENT_SK ;;
  }

  dimension: route_id {
    type: number
    sql: ${TABLE}.ROUTE_ID ;;
  }

  dimension: route_length_miles_flt {
    type: number
    sql: ${TABLE}.ROUTE_LENGTH_MILES_FLT ;;
  }

  dimension: route_name_txt {
    type: string
    sql: ${TABLE}.ROUTE_NAME_TXT ;;
  }

  dimension: route_sk {
    type: number
    sql: ${TABLE}.ROUTE_SK ;;
  }

  dimension: route_start_mm {
    type: number
    sql: ${TABLE}.ROUTE_START_MM ;;
  }

  dimension: route_start_road_id {
    type: number
    sql: ${TABLE}.ROUTE_START_ROAD_ID ;;
  }

  dimension: route_start_road_sk {
    type: number
    sql: ${TABLE}.ROUTE_START_ROAD_SK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
