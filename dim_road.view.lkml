view: dim_road {
  sql_table_name: CTMR_MART.DIM_ROAD ;;

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: road_abbr_txt {
    type: string
    sql: ${TABLE}.ROAD_ABBR_TXT ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: road_order_num {
    type: number
    sql: ${TABLE}.ROAD_ORDER_NUM ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: shield_image_name_txt {
    type: string
    sql: ${TABLE}.SHIELD_IMAGE_NAME_TXT ;;
  }

  dimension: type_road_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_CD ;;
  }

  dimension: type_road_long_desc_txt {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC_TXT ;;
  }

  dimension: type_road_short_desc_txt {
    type: string
    sql: ${TABLE}.TYPE_ROAD_SHORT_DESC_TXT ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
