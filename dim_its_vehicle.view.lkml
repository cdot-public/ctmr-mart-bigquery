view: dim_its_vehicle {
  sql_table_name: CTMR_MART.DIM_ITS_VEHICLE ;;

  dimension_group: adddate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ADDDATE ;;
  }

  dimension: barcode_id {
    type: number
    sql: ${TABLE}.BARCODE_ID ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: editor {
    type: string
    sql: ${TABLE}.EDITOR ;;
  }

  dimension: isenabled {
    type: number
    sql: ${TABLE}.ISENABLED ;;
  }

  dimension: mileage {
    type: number
    sql: ${TABLE}.MILEAGE ;;
  }

  dimension: plate {
    type: string
    sql: ${TABLE}.PLATE ;;
  }

  dimension: sapno {
    type: string
    sql: ${TABLE}.SAPNO ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.UPDATED ;;
  }

  dimension: vehicle {
    type: string
    sql: ${TABLE}.VEHICLE ;;
  }

  dimension: vehicle_id {
    type: number
    sql: ${TABLE}.VEHICLE_ID ;;
  }

  dimension: vehicle_pid {
    type: number
    value_format_name: id
    sql: ${TABLE}.VEHICLE_PID ;;
  }

  dimension: vehicle_sk {
    type: number
    sql: ${TABLE}.VEHICLE_SK ;;
  }

  dimension: vehtype {
    type: string
    sql: ${TABLE}.VEHTYPE ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
