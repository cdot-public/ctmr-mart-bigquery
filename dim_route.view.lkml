view: dim_route {
  sql_table_name: CTMR_MART.DIM_ROUTE ;;

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: end_milemarker_flt {
    type: number
    sql: ${TABLE}.END_MILEMARKER_FLT ;;
  }

  dimension: end_road_id {
    type: number
    sql: ${TABLE}.END_ROAD_ID ;;
  }

  dimension: end_road_name_txt {
    type: string
    sql: ${TABLE}.END_ROAD_NAME_TXT ;;
  }

  dimension: end_type_direction_cd {
    type: number
    sql: ${TABLE}.END_TYPE_DIRECTION_CD ;;
  }

  dimension: end_type_direction_long {
    type: string
    sql: ${TABLE}.END_TYPE_DIRECTION_LONG ;;
  }

  dimension: end_type_direction_short {
    type: string
    sql: ${TABLE}.END_TYPE_DIRECTION_SHORT ;;
  }

  dimension: end_type_direction_sort {
    type: number
    sql: ${TABLE}.END_TYPE_DIRECTION_SORT ;;
  }

  dimension: end_type_direction_value {
    type: string
    sql: ${TABLE}.END_TYPE_DIRECTION_VALUE ;;
  }

  dimension: end_type_road_cd {
    type: number
    sql: ${TABLE}.END_TYPE_ROAD_CD ;;
  }

  dimension: end_type_road_long_desc {
    type: string
    sql: ${TABLE}.END_TYPE_ROAD_LONG_DESC ;;
  }

  dimension: end_type_road_short_desc {
    type: string
    sql: ${TABLE}.END_TYPE_ROAD_SHORT_DESC ;;
  }

  dimension: expected_travel_time_num {
    type: number
    sql: ${TABLE}.EXPECTED_TRAVEL_TIME_NUM ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: route_desc_txt {
    type: string
    sql: ${TABLE}.ROUTE_DESC_TXT ;;
  }

  dimension: route_id {
    type: number
    sql: ${TABLE}.ROUTE_ID ;;
  }

  dimension: route_length_miles_flt {
    type: number
    sql: ${TABLE}.ROUTE_LENGTH_MILES_FLT ;;
  }

  dimension: route_name_txt {
    type: string
    sql: ${TABLE}.ROUTE_NAME_TXT ;;
  }

  dimension: route_sk {
    type: number
    sql: ${TABLE}.ROUTE_SK ;;
  }

  dimension: security_group_id {
    type: number
    sql: ${TABLE}.SECURITY_GROUP_ID ;;
  }

  dimension: security_group_name {
    type: string
    sql: ${TABLE}.SECURITY_GROUP_NAME ;;
  }

  dimension: sort_order_num {
    type: number
    sql: ${TABLE}.SORT_ORDER_NUM ;;
  }

  dimension: start_milemarker_flt {
    type: number
    sql: ${TABLE}.START_MILEMARKER_FLT ;;
  }

  dimension: start_road_id {
    type: number
    sql: ${TABLE}.START_ROAD_ID ;;
  }

  dimension: start_road_name_txt {
    type: string
    sql: ${TABLE}.START_ROAD_NAME_TXT ;;
  }

  dimension: start_type_direction_cd {
    type: number
    sql: ${TABLE}.START_TYPE_DIRECTION_CD ;;
  }

  dimension: start_type_direction_long {
    type: string
    sql: ${TABLE}.START_TYPE_DIRECTION_LONG ;;
  }

  dimension: start_type_direction_short {
    type: string
    sql: ${TABLE}.START_TYPE_DIRECTION_SHORT ;;
  }

  dimension: start_type_direction_sort {
    type: number
    sql: ${TABLE}.START_TYPE_DIRECTION_SORT ;;
  }

  dimension: start_type_direction_value {
    type: string
    sql: ${TABLE}.START_TYPE_DIRECTION_VALUE ;;
  }

  dimension: start_type_road_cd {
    type: number
    sql: ${TABLE}.START_TYPE_ROAD_CD ;;
  }

  dimension: start_type_road_long_desc {
    type: string
    sql: ${TABLE}.START_TYPE_ROAD_LONG_DESC ;;
  }

  dimension: start_type_road_short_desc {
    type: string
    sql: ${TABLE}.START_TYPE_ROAD_SHORT_DESC ;;
  }

  dimension: type_route_disabled_reason_cd {
    type: number
    sql: ${TABLE}.TYPE_ROUTE_DISABLED_REASON_CD ;;
  }

  dimension: type_route_disabled_rsn_long {
    type: string
    sql: ${TABLE}.TYPE_ROUTE_DISABLED_RSN_LONG ;;
  }

  dimension: type_route_disabled_rsn_short {
    type: string
    sql: ${TABLE}.TYPE_ROUTE_DISABLED_RSN_SHORT ;;
  }

  dimension: type_route_disabled_rsn_sort {
    type: number
    sql: ${TABLE}.TYPE_ROUTE_DISABLED_RSN_SORT ;;
  }

  dimension: type_route_disabled_rsn_value {
    type: string
    sql: ${TABLE}.TYPE_ROUTE_DISABLED_RSN_VALUE ;;
  }

  dimension: type_route_status_cd {
    type: number
    sql: ${TABLE}.TYPE_ROUTE_STATUS_CD ;;
  }

  dimension: type_route_status_long {
    type: string
    sql: ${TABLE}.TYPE_ROUTE_STATUS_LONG ;;
  }

  dimension: type_route_status_short {
    type: string
    sql: ${TABLE}.TYPE_ROUTE_STATUS_SHORT ;;
  }

  dimension: type_route_status_sort {
    type: number
    sql: ${TABLE}.TYPE_ROUTE_STATUS_SORT ;;
  }

  dimension: type_route_status_value {
    type: string
    sql: ${TABLE}.TYPE_ROUTE_STATUS_VALUE ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  measure: count {
    type: count
    drill_fields: [security_group_name]
  }
}
