view: dim_tv_camera {
  sql_table_name: CTMR_MART.DIM_TV_CAMERA ;;

  dimension: camera_direction {
    type: string
    sql: ${TABLE}.CAMERA_DIRECTION ;;
  }

  dimension: camera_index {
    type: number
    sql: ${TABLE}.CAMERA_INDEX ;;
  }

  dimension: camera_name {
    type: string
    sql: ${TABLE}.CAMERA_NAME ;;
  }

  dimension: camera_sk {
    type: number
    sql: ${TABLE}.CAMERA_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: latitude {
    type: number
    sql: ${TABLE}.LATITUDE ;;
  }

  dimension: longitude {
    type: number
    sql: ${TABLE}.LONGITUDE ;;
  }

  measure: count {
    type: count
    drill_fields: [camera_name]
  }
}
