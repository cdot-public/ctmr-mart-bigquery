view: fact_audit_event_summary {
  sql_table_name: CTMR_MART.FACT_AUDIT_EVENT_SUMMARY ;;

  dimension: audit_event_summary_id {
    type: number
    sql: ${TABLE}.AUDIT_EVENT_SUMMARY_ID ;;
  }

  dimension: audit_event_summary_sk {
    type: number
    sql: ${TABLE}.AUDIT_EVENT_SUMMARY_SK ;;
  }

  dimension_group: audit_event_summary_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.AUDIT_EVENT_SUMMARY_TS ;;
  }

  dimension_group: end_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.END_DT ;;
  }

  dimension: end_dt_sk {
    type: number
    sql: ${TABLE}.END_DT_SK ;;
  }

  dimension: end_mm {
    type: number
    sql: ${TABLE}.END_MM ;;
  }

  dimension: end_tm_sk {
    type: number
    sql: ${TABLE}.END_TM_SK ;;
  }

  dimension: event_id {
    type: number
    sql: ${TABLE}.EVENT_ID ;;
  }

  dimension: event_sk {
    type: number
    sql: ${TABLE}.EVENT_SK ;;
  }

  dimension: event_sub_type_desc {
    type: string
    sql: ${TABLE}.EVENT_SUB_TYPE_DESC ;;
  }

  dimension: event_sub_type_id {
    type: number
    sql: ${TABLE}.EVENT_SUB_TYPE_ID ;;
  }

  dimension: event_subclass_desc {
    type: string
    sql: ${TABLE}.EVENT_SUBCLASS_DESC ;;
  }

  dimension: event_subclass_id {
    type: number
    sql: ${TABLE}.EVENT_SUBCLASS_ID ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension: isboth_direction_flg {
    type: number
    sql: ${TABLE}.ISBOTH_DIRECTION_FLG ;;
  }

  dimension: road_abbr_txt {
    type: string
    sql: ${TABLE}.ROAD_ABBR_TXT ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension_group: start_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.START_DT ;;
  }

  dimension: start_dt_sk {
    type: number
    sql: ${TABLE}.START_DT_SK ;;
  }

  dimension: start_mm {
    type: number
    sql: ${TABLE}.START_MM ;;
  }

  dimension: start_tm_sk {
    type: number
    sql: ${TABLE}.START_TM_SK ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_DESC ;;
  }

  dimension: type_road_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_CD ;;
  }

  dimension: type_road_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC ;;
  }

  dimension: type_road_short_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_SHORT_DESC ;;
  }

  dimension: type_roadway_closure_cd {
    type: number
    sql: ${TABLE}.TYPE_ROADWAY_CLOSURE_CD ;;
  }

  dimension: type_roadway_closure_desc {
    type: string
    sql: ${TABLE}.TYPE_ROADWAY_CLOSURE_DESC ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
