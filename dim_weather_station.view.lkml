view: dim_weather_station {
  sql_table_name: CTMR_MART.DIM_WEATHER_STATION ;;

  dimension: comm_error_flg {
    type: number
    sql: ${TABLE}.COMM_ERROR_FLG ;;
  }

  dimension: configuration_id_txt {
    type: string
    sql: ${TABLE}.CONFIGURATION_ID_TXT ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: device_id {
    type: number
    sql: ${TABLE}.DEVICE_ID ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: friction_ind {
    type: string
    sql: ${TABLE}.FRICTION_IND ;;
  }

  dimension_group: last_atmos_collection_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_ATMOS_COLLECTION_DT ;;
  }

  dimension_group: last_device_collection_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_DEVICE_COLLECTION_DT ;;
  }

  dimension_group: last_system_collection_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_SYSTEM_COLLECTION_DT ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: type_reason_cd {
    type: number
    sql: ${TABLE}.TYPE_REASON_CD ;;
  }

  dimension: type_reason_long {
    type: string
    sql: ${TABLE}.TYPE_REASON_LONG ;;
  }

  dimension: type_reason_short {
    type: string
    sql: ${TABLE}.TYPE_REASON_SHORT ;;
  }

  dimension: type_reason_sort {
    type: number
    sql: ${TABLE}.TYPE_REASON_SORT ;;
  }

  dimension: type_reason_value {
    type: string
    sql: ${TABLE}.TYPE_REASON_VALUE ;;
  }

  dimension: type_weather_source_cd {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_SOURCE_CD ;;
  }

  dimension: type_weather_source_long {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_SOURCE_LONG ;;
  }

  dimension: type_weather_source_short {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_SOURCE_SHORT ;;
  }

  dimension: type_weather_source_sort {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_SOURCE_SORT ;;
  }

  dimension: type_weather_source_value {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_SOURCE_VALUE ;;
  }

  dimension: type_weather_st_model_cd {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_ST_MODEL_CD ;;
  }

  dimension: type_weather_st_model_long {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_ST_MODEL_LONG ;;
  }

  dimension: type_weather_st_model_short {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_ST_MODEL_SHORT ;;
  }

  dimension: type_weather_st_model_sort {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_ST_MODEL_SORT ;;
  }

  dimension: type_weather_st_model_value {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_ST_MODEL_VALUE ;;
  }

  dimension: type_weather_st_status_cd {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_ST_STATUS_CD ;;
  }

  dimension: type_weather_st_status_long {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_ST_STATUS_LONG ;;
  }

  dimension: type_weather_st_status_short {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_ST_STATUS_SHORT ;;
  }

  dimension: type_weather_st_status_sort {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_ST_STATUS_SORT ;;
  }

  dimension: type_weather_st_status_value {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_ST_STATUS_VALUE ;;
  }

  dimension: type_weather_st_tech_cd {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_ST_TECH_CD ;;
  }

  dimension: type_weather_st_tech_long {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_ST_TECH_LONG ;;
  }

  dimension: type_weather_st_tech_short {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_ST_TECH_SHORT ;;
  }

  dimension: type_weather_st_tech_sort {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_ST_TECH_SORT ;;
  }

  dimension: type_weather_st_tech_value {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_ST_TECH_VALUE ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: weather_station_id {
    type: number
    sql: ${TABLE}.WEATHER_STATION_ID ;;
  }

  dimension: weather_station_sk {
    type: number
    sql: ${TABLE}.WEATHER_STATION_SK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
