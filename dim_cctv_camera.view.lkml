view: dim_cctv_camera {
  sql_table_name: CTMR_MART.DIM_CCTV_CAMERA ;;

  dimension: cctv_camera_desc_txt {
    type: string
    sql: ${TABLE}.CCTV_CAMERA_DESC_TXT ;;
  }

  dimension: cctv_camera_id {
    type: number
    sql: ${TABLE}.CCTV_CAMERA_ID ;;
  }

  dimension: cctv_camera_name_txt {
    type: string
    sql: ${TABLE}.CCTV_CAMERA_NAME_TXT ;;
  }

  dimension: cctv_camera_sk {
    type: number
    sql: ${TABLE}.CCTV_CAMERA_SK ;;
  }

  dimension: cctv_id {
    type: number
    sql: ${TABLE}.CCTV_ID ;;
  }

  dimension: city_id {
    type: number
    sql: ${TABLE}.CITY_ID ;;
  }

  dimension: county_id {
    type: number
    sql: ${TABLE}.COUNTY_ID ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: dgl_latitude_flt {
    type: number
    sql: ${TABLE}.DGL_LATITUDE_FLT ;;
  }

  dimension: dgl_longitude_flt {
    type: number
    sql: ${TABLE}.DGL_LONGITUDE_FLT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: east_position_desc {
    type: string
    sql: ${TABLE}.EAST_POSITION_DESC ;;
  }

  dimension: east_url {
    type: string
    sql: ${TABLE}.EAST_URL ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: latitude_flt {
    type: number
    sql: ${TABLE}.LATITUDE_FLT ;;
  }

  dimension: longitude_flt {
    type: number
    sql: ${TABLE}.LONGITUDE_FLT ;;
  }

  dimension: maintenance_group_id {
    type: number
    sql: ${TABLE}.MAINTENANCE_GROUP_ID ;;
  }

  dimension: maintenance_group_name {
    type: string
    sql: ${TABLE}.MAINTENANCE_GROUP_NAME ;;
  }

  dimension: manufacturer_id {
    type: number
    sql: ${TABLE}.MANUFACTURER_ID ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: north_position_desc {
    type: string
    sql: ${TABLE}.NORTH_POSITION_DESC ;;
  }

  dimension: north_url {
    type: string
    sql: ${TABLE}.NORTH_URL ;;
  }

  dimension: notes_txt {
    type: string
    sql: ${TABLE}.NOTES_TXT ;;
  }

  dimension: public_common_name_txt {
    type: string
    sql: ${TABLE}.PUBLIC_COMMON_NAME_TXT ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: sap_id {
    type: number
    sql: ${TABLE}.SAP_ID ;;
  }

  dimension: sap_name_txt {
    type: string
    sql: ${TABLE}.SAP_NAME_TXT ;;
  }

  dimension: security_group_id {
    type: number
    sql: ${TABLE}.SECURITY_GROUP_ID ;;
  }

  dimension: security_group_name {
    type: string
    sql: ${TABLE}.SECURITY_GROUP_NAME ;;
  }

  dimension: south_position_desc {
    type: string
    sql: ${TABLE}.SOUTH_POSITION_DESC ;;
  }

  dimension: south_url {
    type: string
    sql: ${TABLE}.SOUTH_URL ;;
  }

  dimension: stream_address {
    type: string
    sql: ${TABLE}.STREAM_ADDRESS ;;
  }

  dimension: stream_application {
    type: string
    sql: ${TABLE}.STREAM_APPLICATION ;;
  }

  dimension: stream_name {
    type: string
    sql: ${TABLE}.STREAM_NAME ;;
  }

  dimension: stream_port {
    type: string
    sql: ${TABLE}.STREAM_PORT ;;
  }

  dimension: sync_dsply_actl_loc_flg {
    type: number
    sql: ${TABLE}.SYNC_DSPLY_ACTL_LOC_FLG ;;
  }

  dimension: type_camera_cd {
    type: number
    sql: ${TABLE}.TYPE_CAMERA_CD ;;
  }

  dimension: type_camera_long {
    type: string
    sql: ${TABLE}.TYPE_CAMERA_LONG ;;
  }

  dimension: type_camera_short {
    type: string
    sql: ${TABLE}.TYPE_CAMERA_SHORT ;;
  }

  dimension: type_camera_status_cd {
    type: number
    sql: ${TABLE}.TYPE_CAMERA_STATUS_CD ;;
  }

  dimension: type_camera_status_long {
    type: string
    sql: ${TABLE}.TYPE_CAMERA_STATUS_LONG ;;
  }

  dimension: type_camera_status_short {
    type: string
    sql: ${TABLE}.TYPE_CAMERA_STATUS_SHORT ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_code_value {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_CODE_VALUE ;;
  }

  dimension: type_direction_long_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_LONG_DESC ;;
  }

  dimension: type_direction_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_SHORT_DESC ;;
  }

  dimension: type_group_long {
    type: string
    sql: ${TABLE}.TYPE_GROUP_LONG ;;
  }

  dimension: type_group_short {
    type: string
    sql: ${TABLE}.TYPE_GROUP_SHORT ;;
  }

  dimension: type_road_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_CD ;;
  }

  dimension: type_road_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC ;;
  }

  dimension: type_road_short_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_SHORT_DESC ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  dimension: video_source_url {
    type: string
    sql: ${TABLE}.VIDEO_SOURCE_URL ;;
  }

  dimension: west_position_desc {
    type: string
    sql: ${TABLE}.WEST_POSITION_DESC ;;
  }

  dimension: west_url {
    type: string
    sql: ${TABLE}.WEST_URL ;;
  }

  measure: count {
    type: count
    drill_fields: [maintenance_group_name, security_group_name, stream_name]
  }
}
