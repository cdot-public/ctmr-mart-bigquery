view: fact_tv_incidents {
  sql_table_name: CTMR_MART.FACT_TV_INCIDENTS ;;

  dimension: camera_direction {
    type: string
    sql: ${TABLE}.CAMERA_DIRECTION ;;
  }

  dimension: camera_name {
    type: string
    sql: ${TABLE}.CAMERA_NAME ;;
  }

  dimension: camera_polling_sk {
    type: number
    sql: ${TABLE}.CAMERA_POLLING_SK ;;
  }

  dimension: camera_sk {
    type: number
    sql: ${TABLE}.CAMERA_SK ;;
  }

  dimension: date_sk {
    type: number
    sql: ${TABLE}.DATE_SK ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension_group: incident_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.INCIDENT_DATETIME ;;
  }

  dimension: incident_priority {
    type: number
    sql: ${TABLE}.INCIDENT_PRIORITY ;;
  }

  dimension: incident_sk {
    type: number
    sql: ${TABLE}.INCIDENT_SK ;;
  }

  dimension: incident_snapshot {
    type: string
    sql: ${TABLE}.INCIDENT_SNAPSHOT ;;
  }

  dimension: incident_type {
    type: number
    sql: ${TABLE}.INCIDENT_TYPE ;;
  }

  dimension: incident_type_name {
    type: string
    sql: ${TABLE}.INCIDENT_TYPE_NAME ;;
  }

  dimension: incident_zone {
    type: string
    sql: ${TABLE}.INCIDENT_ZONE ;;
  }

  dimension: milemarker {
    type: number
    sql: ${TABLE}.MILEMARKER ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: time_sk {
    type: number
    sql: ${TABLE}.TIME_SK ;;
  }

  measure: count {
    type: count
    drill_fields: [incident_type_name, camera_name]
  }
}
