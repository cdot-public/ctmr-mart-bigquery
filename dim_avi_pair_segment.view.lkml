view: dim_avi_pair_segment {
  sql_table_name: CTMR_MART.DIM_AVI_PAIR_SEGMENT ;;

  dimension: avi_pair_segment_id {
    type: number
    sql: ${TABLE}.AVI_PAIR_SEGMENT_ID ;;
  }

  dimension: avi_pair_segment_sk {
    type: number
    sql: ${TABLE}.AVI_PAIR_SEGMENT_SK ;;
  }

  dimension: avi_pair_sk {
    type: number
    sql: ${TABLE}.AVI_PAIR_SK ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: segment_direction_cd {
    type: number
    sql: ${TABLE}.SEGMENT_DIRECTION_CD ;;
  }

  dimension: segment_direction_desc {
    type: string
    sql: ${TABLE}.SEGMENT_DIRECTION_DESC ;;
  }

  dimension: segment_length_miles_flt {
    type: number
    sql: ${TABLE}.SEGMENT_LENGTH_MILES_FLT ;;
  }

  dimension: segment_mm_end {
    type: number
    sql: ${TABLE}.SEGMENT_MM_END ;;
  }

  dimension: segment_mm_start {
    type: number
    sql: ${TABLE}.SEGMENT_MM_START ;;
  }

  dimension: segment_name {
    type: string
    sql: ${TABLE}.SEGMENT_NAME ;;
  }

  dimension: segment_order_num {
    type: number
    sql: ${TABLE}.SEGMENT_ORDER_NUM ;;
  }

  dimension: segment_sk {
    type: number
    sql: ${TABLE}.SEGMENT_SK ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  measure: count {
    type: count
    drill_fields: [segment_name]
  }
}
