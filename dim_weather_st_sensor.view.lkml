view: dim_weather_st_sensor {
  sql_table_name: CTMR_MART.DIM_WEATHER_ST_SENSOR ;;

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: friction_ind {
    type: string
    sql: ${TABLE}.FRICTION_IND ;;
  }

  dimension_group: last_device_collection_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_DEVICE_COLLECTION_DT ;;
  }

  dimension_group: last_system_collection_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_SYSTEM_COLLECTION_DT ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: sensor_id_num {
    type: number
    value_format_name: id
    sql: ${TABLE}.SENSOR_ID_NUM ;;
  }

  dimension: sensor_name_txt {
    type: string
    sql: ${TABLE}.SENSOR_NAME_TXT ;;
  }

  dimension: site_id_num {
    type: number
    value_format_name: id
    sql: ${TABLE}.SITE_ID_NUM ;;
  }

  dimension: type_weather_sensor_cd {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_SENSOR_CD ;;
  }

  dimension: type_weather_sensor_long {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_SENSOR_LONG ;;
  }

  dimension: type_weather_sensor_short {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_SENSOR_SHORT ;;
  }

  dimension: type_weather_sensor_sort {
    type: number
    sql: ${TABLE}.TYPE_WEATHER_SENSOR_SORT ;;
  }

  dimension: type_weather_sensor_value {
    type: string
    sql: ${TABLE}.TYPE_WEATHER_SENSOR_VALUE ;;
  }

  dimension: type_ws_status_cd {
    type: number
    sql: ${TABLE}.TYPE_WS_STATUS_CD ;;
  }

  dimension: type_ws_status_long {
    type: string
    sql: ${TABLE}.TYPE_WS_STATUS_LONG ;;
  }

  dimension: type_ws_status_short {
    type: string
    sql: ${TABLE}.TYPE_WS_STATUS_SHORT ;;
  }

  dimension: type_ws_status_sort {
    type: number
    sql: ${TABLE}.TYPE_WS_STATUS_SORT ;;
  }

  dimension: type_ws_status_value {
    type: string
    sql: ${TABLE}.TYPE_WS_STATUS_VALUE ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: weather_station_id {
    type: number
    sql: ${TABLE}.WEATHER_STATION_ID ;;
  }

  dimension: weather_station_sensor_id {
    type: number
    sql: ${TABLE}.WEATHER_STATION_SENSOR_ID ;;
  }

  dimension: weather_station_sensor_sk {
    type: number
    sql: ${TABLE}.WEATHER_STATION_SENSOR_SK ;;
  }

  dimension: weather_station_sk {
    type: number
    sql: ${TABLE}.WEATHER_STATION_SK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
