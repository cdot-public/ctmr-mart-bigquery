view: dim_type_agency_no_service {
  sql_table_name: CTMR_MART.DIM_TYPE_AGENCY_NO_SERVICE ;;

  dimension: code_value_txt {
    type: string
    sql: ${TABLE}.CODE_VALUE_TXT ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_begin {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_BEGIN_DATE ;;
  }

  dimension_group: dim_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_END_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension: long_desc_txt {
    type: string
    sql: ${TABLE}.LONG_DESC_TXT ;;
  }

  dimension: short_desc_txt {
    type: string
    sql: ${TABLE}.SHORT_DESC_TXT ;;
  }

  dimension: sort_order_num {
    type: number
    sql: ${TABLE}.SORT_ORDER_NUM ;;
  }

  dimension: type_agency_no_service_cd {
    type: number
    sql: ${TABLE}.TYPE_AGENCY_NO_SERVICE_CD ;;
  }

  dimension: type_agency_no_service_sk {
    type: number
    sql: ${TABLE}.TYPE_AGENCY_NO_SERVICE_SK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
