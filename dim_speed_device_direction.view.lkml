view: dim_speed_device_direction {
  sql_table_name: CTMR_MART.DIM_SPEED_DEVICE_DIRECTION ;;

  dimension: capacity {
    type: number
    sql: ${TABLE}.CAPACITY ;;
  }

  dimension: common_name_txt {
    type: string
    sql: ${TABLE}.COMMON_NAME_TXT ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: device_direction_cd {
    type: number
    sql: ${TABLE}.DEVICE_DIRECTION_CD ;;
  }

  dimension: device_direction_code_value {
    type: string
    sql: ${TABLE}.DEVICE_DIRECTION_CODE_VALUE ;;
  }

  dimension: device_direction_long_desc {
    type: string
    sql: ${TABLE}.DEVICE_DIRECTION_LONG_DESC ;;
  }

  dimension: device_direction_short_desc {
    type: string
    sql: ${TABLE}.DEVICE_DIRECTION_SHORT_DESC ;;
  }

  dimension: device_id {
    type: number
    sql: ${TABLE}.DEVICE_ID ;;
  }

  dimension: device_sk {
    type: number
    sql: ${TABLE}.DEVICE_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: group_id {
    type: number
    sql: ${TABLE}.GROUP_ID ;;
  }

  dimension: in_use_flg {
    type: number
    sql: ${TABLE}.IN_USE_FLG ;;
  }

  dimension: ipaddress_txt {
    type: string
    sql: ${TABLE}.IPADDRESS_TXT ;;
  }

  dimension_group: last_poll_time_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_POLL_TIME_DT ;;
  }

  dimension: latitude_flt {
    type: number
    sql: ${TABLE}.LATITUDE_FLT ;;
  }

  dimension: longitude_flt {
    type: number
    sql: ${TABLE}.LONGITUDE_FLT ;;
  }

  dimension: milemarker_flt {
    type: number
    sql: ${TABLE}.MILEMARKER_FLT ;;
  }

  dimension: polling_enabled_flg {
    type: number
    sql: ${TABLE}.POLLING_ENABLED_FLG ;;
  }

  dimension: polling_interval_num {
    type: number
    sql: ${TABLE}.POLLING_INTERVAL_NUM ;;
  }

  dimension: port_num {
    type: number
    sql: ${TABLE}.PORT_NUM ;;
  }

  dimension: post_ttt_enabled_flg {
    type: number
    sql: ${TABLE}.POST_TTT_ENABLED_FLG ;;
  }

  dimension: primary_segment_offset_num {
    type: number
    sql: ${TABLE}.PRIMARY_SEGMENT_OFFSET_NUM ;;
  }

  dimension: publish_to_web_flg {
    type: number
    sql: ${TABLE}.PUBLISH_TO_WEB_FLG ;;
  }

  dimension: reading_direction_cd {
    type: number
    sql: ${TABLE}.READING_DIRECTION_CD ;;
  }

  dimension: reading_direction_desc {
    type: string
    sql: ${TABLE}.READING_DIRECTION_DESC ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_id {
    type: number
    sql: ${TABLE}.ROAD_ID ;;
  }

  dimension: road_name_txt {
    type: string
    sql: ${TABLE}.ROAD_NAME_TXT ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: secondary_segment_offset_num {
    type: number
    sql: ${TABLE}.SECONDARY_SEGMENT_OFFSET_NUM ;;
  }

  dimension: security_group_name {
    type: string
    sql: ${TABLE}.SECURITY_GROUP_NAME ;;
  }

  dimension: speed_device_direction_sk {
    type: number
    sql: ${TABLE}.SPEED_DEVICE_DIRECTION_SK ;;
  }

  dimension: type_device_cd {
    type: number
    sql: ${TABLE}.TYPE_DEVICE_CD ;;
  }

  dimension: type_device_code_value {
    type: string
    sql: ${TABLE}.TYPE_DEVICE_CODE_VALUE ;;
  }

  dimension: type_device_long_desc {
    type: string
    sql: ${TABLE}.TYPE_DEVICE_LONG_DESC ;;
  }

  dimension: type_device_short_desc {
    type: string
    sql: ${TABLE}.TYPE_DEVICE_SHORT_DESC ;;
  }

  dimension: type_group_long {
    type: string
    sql: ${TABLE}.TYPE_GROUP_LONG ;;
  }

  dimension: type_group_short {
    type: string
    sql: ${TABLE}.TYPE_GROUP_SHORT ;;
  }

  dimension: type_road_cd {
    type: number
    sql: ${TABLE}.TYPE_ROAD_CD ;;
  }

  dimension: type_road_long_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_LONG_DESC ;;
  }

  dimension: type_road_short_desc {
    type: string
    sql: ${TABLE}.TYPE_ROAD_SHORT_DESC ;;
  }

  measure: count {
    type: count
    drill_fields: [security_group_name]
  }
}
