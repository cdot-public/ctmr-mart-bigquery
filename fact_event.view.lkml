view: fact_event {
  sql_table_name: CTMR_MART.FACT_EVENT ;;

  dimension: cctv_camera_id {
    type: number
    sql: ${TABLE}.CCTV_CAMERA_ID ;;
  }

  dimension: cmv_flg {
    type: number
    sql: ${TABLE}.CMV_FLG ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DT ;;
  }

  dimension: create_user_txt {
    type: string
    sql: ${TABLE}.CREATE_USER_TXT ;;
  }

  dimension: crossroad_sk {
    type: number
    sql: ${TABLE}.CROSSROAD_SK ;;
  }

  dimension_group: end_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.END_DT ;;
  }

  dimension: end_dt_sk {
    type: number
    sql: ${TABLE}.END_DT_SK ;;
  }

  dimension: end_tm_sk {
    type: number
    sql: ${TABLE}.END_TM_SK ;;
  }

  dimension: est_time_to_clear_txt {
    type: string
    sql: ${TABLE}.EST_TIME_TO_CLEAR_TXT ;;
  }

  dimension: event_id {
    type: number
    sql: ${TABLE}.EVENT_ID ;;
  }

  dimension: event_sk {
    type: number
    sql: ${TABLE}.EVENT_SK ;;
  }

  dimension: event_sub_type_headline {
    type: string
    sql: ${TABLE}.EVENT_SUB_TYPE_HEADLINE ;;
  }

  dimension: event_sub_type_id {
    type: number
    sql: ${TABLE}.EVENT_SUB_TYPE_ID ;;
  }

  dimension: event_sub_type_sort {
    type: number
    sql: ${TABLE}.EVENT_SUB_TYPE_SORT ;;
  }

  dimension: event_subclass_desc {
    type: string
    sql: ${TABLE}.EVENT_SUBCLASS_DESC ;;
  }

  dimension_group: event_subclass_end_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.EVENT_SUBCLASS_END_DT ;;
  }

  dimension: event_subclass_id {
    type: number
    sql: ${TABLE}.EVENT_SUBCLASS_ID ;;
  }

  dimension_group: event_subclass_start_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.EVENT_SUBCLASS_START_DT ;;
  }

  dimension: fact_active_flag {
    type: string
    sql: ${TABLE}.FACT_ACTIVE_FLAG ;;
  }

  dimension_group: fact_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_DELETE_DATE ;;
  }

  dimension_group: fact_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_INSERT_DATE ;;
  }

  dimension: fact_md5_hash_value {
    type: string
    sql: ${TABLE}.FACT_MD5_HASH_VALUE ;;
  }

  dimension: fact_uid_ind {
    type: string
    sql: ${TABLE}.FACT_UID_IND ;;
  }

  dimension_group: fact_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FACT_UPDATE_DATE ;;
  }

  dimension: fatality_flg {
    type: number
    sql: ${TABLE}.FATALITY_FLG ;;
  }

  dimension: fatality_num {
    type: number
    sql: ${TABLE}.FATALITY_NUM ;;
  }

  dimension_group: first_responder_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FIRST_RESPONDER_DT ;;
  }

  dimension: hazmat_flg {
    type: number
    sql: ${TABLE}.HAZMAT_FLG ;;
  }

  dimension: isboth_direction_flg {
    type: number
    sql: ${TABLE}.ISBOTH_DIRECTION_FLG ;;
  }

  dimension_group: last_update_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DT ;;
  }

  dimension: latitude_flt {
    type: number
    sql: ${TABLE}.LATITUDE_FLT ;;
  }

  dimension: location_desc_txt {
    type: string
    sql: ${TABLE}.LOCATION_DESC_TXT ;;
  }

  dimension: longitude_flt {
    type: number
    sql: ${TABLE}.LONGITUDE_FLT ;;
  }

  dimension: major_event_flg {
    type: number
    sql: ${TABLE}.MAJOR_EVENT_FLG ;;
  }

  dimension: mm_end_flt {
    type: number
    sql: ${TABLE}.MM_END_FLT ;;
  }

  dimension: mm_start_flt {
    type: number
    sql: ${TABLE}.MM_START_FLT ;;
  }

  dimension: notes_txt {
    type: string
    sql: ${TABLE}.NOTES_TXT ;;
  }

  dimension: polyline_id {
    type: number
    sql: ${TABLE}.POLYLINE_ID ;;
  }

  dimension: road_direction_sk {
    type: number
    sql: ${TABLE}.ROAD_DIRECTION_SK ;;
  }

  dimension: road_sk {
    type: number
    sql: ${TABLE}.ROAD_SK ;;
  }

  dimension: roadway_name_txt {
    type: string
    sql: ${TABLE}.ROADWAY_NAME_TXT ;;
  }

  dimension: security_group_id {
    type: number
    sql: ${TABLE}.SECURITY_GROUP_ID ;;
  }

  dimension: security_group_name {
    type: string
    sql: ${TABLE}.SECURITY_GROUP_NAME ;;
  }

  dimension_group: stage_open_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.STAGE_OPEN_DT ;;
  }

  dimension_group: start_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.START_DT ;;
  }

  dimension: start_dt_sk {
    type: number
    sql: ${TABLE}.START_DT_SK ;;
  }

  dimension: start_tm_sk {
    type: number
    sql: ${TABLE}.START_TM_SK ;;
  }

  dimension: type_direction_cd {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_CD ;;
  }

  dimension: type_direction_long {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_LONG ;;
  }

  dimension: type_direction_short {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_SHORT ;;
  }

  dimension: type_direction_sort {
    type: number
    sql: ${TABLE}.TYPE_DIRECTION_SORT ;;
  }

  dimension: type_direction_value {
    type: string
    sql: ${TABLE}.TYPE_DIRECTION_VALUE ;;
  }

  dimension: type_event_category_cd {
    type: number
    sql: ${TABLE}.TYPE_EVENT_CATEGORY_CD ;;
  }

  dimension: type_event_category_long {
    type: string
    sql: ${TABLE}.TYPE_EVENT_CATEGORY_LONG ;;
  }

  dimension: type_event_category_short {
    type: string
    sql: ${TABLE}.TYPE_EVENT_CATEGORY_SHORT ;;
  }

  dimension: type_event_category_sort {
    type: number
    sql: ${TABLE}.TYPE_EVENT_CATEGORY_SORT ;;
  }

  dimension: type_event_category_value {
    type: string
    sql: ${TABLE}.TYPE_EVENT_CATEGORY_VALUE ;;
  }

  dimension: type_event_cd {
    type: number
    sql: ${TABLE}.TYPE_EVENT_CD ;;
  }

  dimension: type_event_level_cd {
    type: number
    sql: ${TABLE}.TYPE_EVENT_LEVEL_CD ;;
  }

  dimension: type_event_level_long {
    type: string
    sql: ${TABLE}.TYPE_EVENT_LEVEL_LONG ;;
  }

  dimension: type_event_level_short {
    type: string
    sql: ${TABLE}.TYPE_EVENT_LEVEL_SHORT ;;
  }

  dimension: type_event_level_sort {
    type: number
    sql: ${TABLE}.TYPE_EVENT_LEVEL_SORT ;;
  }

  dimension: type_event_level_value {
    type: string
    sql: ${TABLE}.TYPE_EVENT_LEVEL_VALUE ;;
  }

  dimension: type_event_long {
    type: string
    sql: ${TABLE}.TYPE_EVENT_LONG ;;
  }

  dimension: type_event_severity_cd {
    type: number
    sql: ${TABLE}.TYPE_EVENT_SEVERITY_CD ;;
  }

  dimension: type_event_severity_long {
    type: string
    sql: ${TABLE}.TYPE_EVENT_SEVERITY_LONG ;;
  }

  dimension: type_event_severity_short {
    type: string
    sql: ${TABLE}.TYPE_EVENT_SEVERITY_SHORT ;;
  }

  dimension: type_event_severity_sort {
    type: number
    sql: ${TABLE}.TYPE_EVENT_SEVERITY_SORT ;;
  }

  dimension: type_event_severity_value {
    type: string
    sql: ${TABLE}.TYPE_EVENT_SEVERITY_VALUE ;;
  }

  dimension: type_event_short {
    type: string
    sql: ${TABLE}.TYPE_EVENT_SHORT ;;
  }

  dimension: type_event_sort {
    type: number
    sql: ${TABLE}.TYPE_EVENT_SORT ;;
  }

  dimension: type_event_source_cd {
    type: number
    sql: ${TABLE}.TYPE_EVENT_SOURCE_CD ;;
  }

  dimension: type_event_source_long {
    type: string
    sql: ${TABLE}.TYPE_EVENT_SOURCE_LONG ;;
  }

  dimension: type_event_source_short {
    type: string
    sql: ${TABLE}.TYPE_EVENT_SOURCE_SHORT ;;
  }

  dimension: type_event_source_sort {
    type: number
    sql: ${TABLE}.TYPE_EVENT_SOURCE_SORT ;;
  }

  dimension: type_event_source_value {
    type: string
    sql: ${TABLE}.TYPE_EVENT_SOURCE_VALUE ;;
  }

  dimension: type_event_status_cd {
    type: number
    sql: ${TABLE}.TYPE_EVENT_STATUS_CD ;;
  }

  dimension: type_event_status_long {
    type: string
    sql: ${TABLE}.TYPE_EVENT_STATUS_LONG ;;
  }

  dimension: type_event_status_short {
    type: string
    sql: ${TABLE}.TYPE_EVENT_STATUS_SHORT ;;
  }

  dimension: type_event_status_sort {
    type: number
    sql: ${TABLE}.TYPE_EVENT_STATUS_SORT ;;
  }

  dimension: type_event_status_value {
    type: string
    sql: ${TABLE}.TYPE_EVENT_STATUS_VALUE ;;
  }

  dimension: type_event_value {
    type: string
    sql: ${TABLE}.TYPE_EVENT_VALUE ;;
  }

  dimension: type_lane_control_cd {
    type: number
    sql: ${TABLE}.TYPE_LANE_CONTROL_CD ;;
  }

  dimension: type_lane_control_long {
    type: string
    sql: ${TABLE}.TYPE_LANE_CONTROL_LONG ;;
  }

  dimension: type_lane_control_short {
    type: string
    sql: ${TABLE}.TYPE_LANE_CONTROL_SHORT ;;
  }

  dimension: type_lane_control_sort {
    type: number
    sql: ${TABLE}.TYPE_LANE_CONTROL_SORT ;;
  }

  dimension: type_lane_control_value {
    type: string
    sql: ${TABLE}.TYPE_LANE_CONTROL_VALUE ;;
  }

  dimension: type_roadway_closure_cd {
    type: number
    sql: ${TABLE}.TYPE_ROADWAY_CLOSURE_CD ;;
  }

  dimension: type_roadway_closure_long {
    type: string
    sql: ${TABLE}.TYPE_ROADWAY_CLOSURE_LONG ;;
  }

  dimension: type_roadway_closure_short {
    type: string
    sql: ${TABLE}.TYPE_ROADWAY_CLOSURE_SHORT ;;
  }

  dimension: type_roadway_closure_sort {
    type: number
    sql: ${TABLE}.TYPE_ROADWAY_CLOSURE_SORT ;;
  }

  dimension: type_roadway_closure_value {
    type: string
    sql: ${TABLE}.TYPE_ROADWAY_CLOSURE_VALUE ;;
  }

  dimension: type_ta_headline_cd {
    type: number
    sql: ${TABLE}.TYPE_TA_HEADLINE_CD ;;
  }

  dimension: type_ta_headline_long {
    type: string
    sql: ${TABLE}.TYPE_TA_HEADLINE_LONG ;;
  }

  dimension: type_ta_headline_short {
    type: string
    sql: ${TABLE}.TYPE_TA_HEADLINE_SHORT ;;
  }

  dimension: update_user_txt {
    type: string
    sql: ${TABLE}.UPDATE_USER_TXT ;;
  }

  dimension: user_lat_lon_flg {
    type: number
    sql: ${TABLE}.USER_LAT_LON_FLG ;;
  }

  dimension: version_num {
    type: number
    sql: ${TABLE}.VERSION_NUM ;;
  }

  measure: count {
    type: count
    drill_fields: [security_group_name]
  }
}
