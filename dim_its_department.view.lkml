view: dim_its_department {
  sql_table_name: CTMR_MART.DIM_ITS_DEPARTMENT ;;

  dimension: autoresp_email {
    type: string
    sql: ${TABLE}.AUTORESP_EMAIL ;;
  }

  dimension: autoresp_email_id {
    type: number
    sql: ${TABLE}.AUTORESP_EMAIL_ID ;;
  }

  dimension: autoresp_email_name {
    type: string
    sql: ${TABLE}.AUTORESP_EMAIL_NAME ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATED ;;
  }

  dimension: dept_id {
    type: number
    sql: ${TABLE}.DEPT_ID ;;
  }

  dimension: dept_name {
    type: string
    sql: ${TABLE}.DEPT_NAME ;;
  }

  dimension: dept_sk {
    type: number
    sql: ${TABLE}.DEPT_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_delete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_DELETE_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension_group: dim_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_UPDATE_DATE ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.EMAIL ;;
  }

  dimension: email_id {
    type: number
    sql: ${TABLE}.EMAIL_ID ;;
  }

  dimension: email_name {
    type: string
    sql: ${TABLE}.EMAIL_NAME ;;
  }

  dimension: group_membership {
    type: number
    sql: ${TABLE}.GROUP_MEMBERSHIP ;;
  }

  dimension: ispublic {
    type: number
    sql: ${TABLE}.ISPUBLIC ;;
  }

  dimension: manager_first_name {
    type: string
    sql: ${TABLE}.MANAGER_FIRST_NAME ;;
  }

  dimension: manager_id {
    type: number
    sql: ${TABLE}.MANAGER_ID ;;
  }

  dimension: manager_last_name {
    type: string
    sql: ${TABLE}.MANAGER_LAST_NAME ;;
  }

  dimension: manager_name {
    type: string
    sql: ${TABLE}.MANAGER_NAME ;;
  }

  dimension: message_auto_response {
    type: number
    sql: ${TABLE}.MESSAGE_AUTO_RESPONSE ;;
  }

  dimension: sla_id {
    type: number
    sql: ${TABLE}.SLA_ID ;;
  }

  dimension: sla_name {
    type: string
    sql: ${TABLE}.SLA_NAME ;;
  }

  dimension: ticket_auto_response {
    type: number
    sql: ${TABLE}.TICKET_AUTO_RESPONSE ;;
  }

  dimension: tpl_id {
    type: number
    sql: ${TABLE}.TPL_ID ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.UPDATED ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      dept_name,
      sla_name,
      autoresp_email_name,
      manager_first_name,
      email_name,
      manager_name,
      manager_last_name
    ]
  }
}
