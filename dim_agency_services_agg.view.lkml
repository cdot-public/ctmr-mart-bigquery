view: dim_agency_services_agg {
  sql_table_name: CTMR_MART.DIM_AGENCY_SERVICES_AGG ;;

  dimension: agency_services_agg_sk {
    type: number
    sql: ${TABLE}.AGENCY_SERVICES_AGG_SK ;;
  }

  dimension: dim_active_flag {
    type: string
    sql: ${TABLE}.DIM_ACTIVE_FLAG ;;
  }

  dimension_group: dim_begin {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_BEGIN_DATE ;;
  }

  dimension_group: dim_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_END_DATE ;;
  }

  dimension_group: dim_insert {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DIM_INSERT_DATE ;;
  }

  dimension: dim_md5_hash_value {
    type: string
    sql: ${TABLE}.DIM_MD5_HASH_VALUE ;;
  }

  dimension: dim_uid_ind {
    type: string
    sql: ${TABLE}.DIM_UID_IND ;;
  }

  dimension: num_of_svc {
    type: number
    sql: ${TABLE}.NUM_OF_SVC ;;
  }

  dimension: svc_desc {
    type: string
    sql: ${TABLE}.SVC_DESC ;;
  }

  dimension: svc_id {
    type: string
    sql: ${TABLE}.SVC_ID ;;
  }

  dimension: svc_rnk {
    type: number
    sql: ${TABLE}.SVC_RNK ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
