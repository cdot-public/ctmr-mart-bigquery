view: geography {
  sql_table_name: segments.geography ;;

  dimension: coordinates {
    hidden: yes
    sql: ${TABLE}.coordinates ;;
  }

  dimension: geography_type {
    type: string
    sql: ${TABLE}.geographyType ;;
  }

  dimension: segment_id {
    type: number
    sql: ${TABLE}.segmentId ;;
  }

  dimension: segment_name {
    type: string
    sql: ${TABLE}.segmentName ;;
  }

  measure: count {
    type: count
    drill_fields: [segment_name]
  }
}

view: geography__coordinates {
  dimension: lat {
    type: number
    sql: ${TABLE}.lat ;;
  }

  dimension: long {
    type: number
    sql: ${TABLE}.long ;;
  }
  dimension: location {
    type: location
    sql_latitude: ${lat} ;;
    sql_longitude: ${long} ;;
  }
}
